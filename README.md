# How to use this Repository
This is an MIT licensed repository of Unity scripts. Most of the scripts are made as self-contained as possible and should be scripts you can just add to your Unity projects and use them immediately. Some of the scripts will have "Abstract" classes as well, which are needed to use the other scripts.

An example of this would be in the Triggers folder. You will find the `AbstractSimpleTrigger` class which is required to make use of the `SimpleBoxTrigger`, `SimpleSphereTrigger`, `SimpleCapsuleTrigger` and `SimpleMeshTrigger` classes.

# Extensions
The Extension folder holds static classes that adds utility functions for different things such as `Lists`, `Enumerables`, etc.

# Telemetry (Really networking in general)
Telemetry is often a topic that few seem to know a lot about in the gamedev community when they haven't been in the game (ha..ha..) for very long. Just the whole concept of sending data "over the wire" as it's called. Normally when you send data from a client (see: the players, typically) you should never include any sensitive information in your code that could let some 3rd party access your backend unlawfully. You **NEVER** connect a client directly to a database. You always have a layer in-between that can take requests from a client using some sort of public/private token setup or other similar methods. But that topic is too big to cover in a Readme file.

# Suggestions and Bugs
Please submit issues on this repo if you find bugs or have suggestions. You can also add suggetions on my ko-fi page.

# Gallery

## IMGUI Dialogue Editor
[<img height='512' src='https://i.imgur.com/LjB5qAF.png'>]()

## Save Game Visualizer
[<img height='512' src='https://i.imgur.com/cA5rWDq.png'>]()
