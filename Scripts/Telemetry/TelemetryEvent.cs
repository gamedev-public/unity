﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JGDT.Telemetry
{
    /// <summary>
    /// All types of Events you want to log with your <see cref="TelemetryData"/>. Expand as needed.
    /// </summary>
    public enum TelemetryEvent
    {
        GameStart,
        PlayerInteract,
        GameEnd
    }
}
