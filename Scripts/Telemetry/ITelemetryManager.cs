﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JGDT.Telemetry
{
    /// <summary>
    /// A TelemetryManager interface that secures streamlined Telemetry Management.
    /// </summary>
    public interface ITelemetryManager
    {
        public void StartSession();
        public void EndSession();
        public void SendData(TelemetryData data);
    }
}
