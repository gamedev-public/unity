﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JGDT.Telemetry
{
    /// <summary>
    /// This is a simple Telemetry data class which can be used to log useful information about your game to develop it further.
    /// Expand it to fit your needs.
    /// </summary>
    public class TelemetryData : AbstractTelemetryData
    {
        internal float TimeStamp = 0f;
        public TelemetryEvent EventName;
        public object Data;

        /// <summary>
        /// Default <see cref="TelemetryData"/> Constructor
        /// </summary>
        /// <param name="eventName">The name of the event expressed as a <see cref="TelemetryEvent"/>.</param>
        /// <param name="data">The data to send.</param>
        public TelemetryData(TelemetryEvent eventName, object data)
        {
            EventName = eventName;
            Data = data;
        }

        /// <summary>
        /// An extended <see cref="TelemetryData"/> constructor.
        /// </summary>
        /// <param name="timeStamp">The in-game timestamp to pass along with the <see cref="TelemetryData"/>.</param>
        /// <param name="eventName">The name of the event expressed as a <see cref="TelemetryEvent"/>.</param>
        /// <param name="data">The data to send.</param>
        internal TelemetryData(float timeStamp, TelemetryEvent eventName, object data)
        {
            TimeStamp = timeStamp;
            EventName = eventName;
            Data = data;
        }

        /// <summary>
        /// An override of ToString to get a JSON friendly string.
        /// </summary>
        /// <returns>Jsonified string of this object.</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append($"'timestamp':'{TimeStamp}',");
            sb.Append($"'eventname':'{Enum.GetName(typeof(TelemetryEvent), EventName)}',");
            sb.Append($"'data':'{Data}'");
            sb.Append("}");
            return sb.ToString();
        }
    }
}
