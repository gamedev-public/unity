﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JGDT.Telemetry
{
    /// <summary>
    /// A base class for <see cref="TelemetryData"/> to enforce sensible <see cref="ToString"/> implementations.
    /// </summary>
    public abstract class AbstractTelemetryData
    {
        public abstract override string ToString();
    }
}
