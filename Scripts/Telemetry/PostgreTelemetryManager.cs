﻿using System;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace JGDT.Telemetry
{
    /// <summary>
    /// This is an example implementation of the <see cref="ITelemetryManager"/> interface, using PostgreSQL.
    /// Make sure to read the bottom of this file.
    /// </summary>
    public class PostgreTelemetryManager : ITelemetryManager
    {
        /// <summary>
        /// This is the format of a PostgreSQL connection string. It should be placed in a config/secret file.
        /// Do NOT save credentials in code and DO NOT commit credentials to your repositories.
        /// </summary>
        private readonly string _connectionString =
            "Host=;" +
            "Port=;" +
            "Database=;" +
            "Username=;" +
            "Password=;" +
            "SSL Mode=;" +
            "Trust Server Certificate=";

        /// <summary>
        /// Using a <see cref="Stopwatch"/> to keep track of time from when the sessison is started.
        /// This can be changed to fit your implementation. In this implementation it is assumed that 
        /// <see cref="ITelemetryManager.StartSession"/> is called at the start of the game.
        /// </summary>
        private static Stopwatch _timer;
        /// <summary>
        /// The ID of this session.
        /// </summary>
        private static Guid _sessionId = Guid.Empty;

        /// <summary>
        /// Should always be called first when starting a new play session that requires Telemetry.
        /// </summary>
        public void StartSession()
        {
            if (_sessionId != Guid.Empty)
            {
                Debug.LogWarning("StartSession has already been called.");
            }
            else
            {
                TelemetryData data = new TelemetryData(0f, TelemetryEvent.GameStart, null);
                _sessionId = Guid.NewGuid();
                _timer = Stopwatch.StartNew();
                SendData(data);
            }
        }

        /// <summary>
        /// Should always be called when a play session can be considered completed.
        /// </summary>
        public void EndSession()
        {
            if (_sessionId == Guid.Empty)
            {
                throw new InvalidOperationException("Cannot call EndSession before having called StartSession.");
            }
            else
            {
                _timer.Stop();
                TelemetryData data = new TelemetryData(_timer.Elapsed.Seconds, TelemetryEvent.GameEnd, null);
                SendData(data);
            }
        }

        /// <summary>
        /// Sends data to the desired service.
        /// </summary>
        /// <param name="data">The <see cref="TelemetryData"/> to send.</param>
        public void SendData(TelemetryData data)
        {
            if (_sessionId == Guid.Empty)
            {
                throw new InvalidOperationException("Cannot call Send before having called StartSession.");
            }
            else
            {
                // Only do this if the object was constructed without a timestamp.
                if (data.TimeStamp == 0f) data.TimeStamp = (float)_timer.Elapsed.TotalSeconds;
                if (data.EventName == TelemetryEvent.GameStart)
                {
                    using (NpgsqlConnection conn = new NpgsqlConnection(_connectionString))
                    {
                        conn.Open();
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText =
                                "INSERT INTO play_sessions(id, play_date)" +
                                $"VALUES('{_sessionId}', '{DateTime.Now.ToString("yyyy-MM-dd")}')";
                            cmd.ExecuteNonQuery();
                        }

                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            if (data.EventName == TelemetryEvent.GameEnd)
                            {
                                cmd.CommandText = ToInsertString(data);
                                _sessionId = Guid.Empty;
                            }
                            else
                            {
                                cmd.CommandText = ToInsertString(data);
                            }
                            cmd.ExecuteNonQuery();
                        }
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        /// A convenience method to make an Insert statement for SQL execution.
        /// </summary>
        /// <param name="data">The <see cref="TelemetryData"/> to base the string on.</param>
        /// <returns>An Insert statement to use in a PostgreSQL database.</returns>
        private string ToInsertString(TelemetryData data) =>
                "INSERT INTO events(play_session_id, timestamp, event_type, event_data)" +
                $"VALUES('{_sessionId}','{data.TimeStamp}', " +
                $"'{Enum.GetName(typeof(TelemetryEvent), data.EventName)}', '{data.Data}');";

        /*
         * Below is a bunch of code that only exists to make this code compile.
         * If you choose to use this example implementation, delete this bottom region
         * and get the Npgsql package to add to your project instead. It is done this way
         * to make sure depedencies won't screw up importing this script.
         */
        #region Npgsql Mocking
        public class NpgsqlConnection : IDisposable
        {
            public NpgsqlConnection(string connectionString) { }
            public void Dispose()
            {
                throw new NotImplementedException();
            }
            public void Open() { }
            public void Close() { }
            public NpgsqlCommand CreateCommand() { return null; }
        }

        public class NpgsqlCommand : IDisposable
        {
            public string CommandText { get; set; }
            public void ExecuteNonQuery() { }
            public void Dispose()
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}
