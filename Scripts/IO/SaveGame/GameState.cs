﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace JGDT.IO.SaveGame
{
    /// <summary>
    /// This class represents a persisted game state.
    /// Has Newtonsoft as a dependency: https://github.com/jilleJr/Newtonsoft.Json-for-Unity
    /// </summary>
    [Serializable]
    public class GameState : IComparable
    {
        public readonly DateTime SaveDate;
        public readonly Dictionary<string, string> Entries;
        public GameState()
        {
            Entries = new Dictionary<string, string>();
            SaveDate = DateTime.Now;
        }

        public GameState(GameState state)
        {
            Entries = state.Entries;
            SaveDate = state.SaveDate;
        }

        /// <summary>
        /// Get specific key from the <see cref="GameState"/>.
        /// </summary>
        /// <param name="key">The name of the key to retrieve</param>
        /// <returns>The specified key as its intended type of object.</returns>
        public T GetKey<T>(string key)
        {
            if (Entries.TryGetValue(key, out string base64Entry))
            {
                byte[] baseEncodedBytes = Convert.FromBase64String(base64Entry);
                return JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(baseEncodedBytes));
            }
            else
            {
                throw new ArgumentException($"Invalid key '{key}'.");
            }
        }

        /// <summary>
        /// Set specific key in the <see cref="GameState"/>
        /// </summary>
        /// <typeparam name="T">The type of key to store.</typeparam>
        /// <param name="key">The unique name of the key to store.</param>
        /// <param name="value">The object value of the key to store.</param>
        public void SetKey<T>(string key, T value)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(value));
            if (Entries.TryGetValue(key, out string entry))
            {
                entry = Convert.ToBase64String(plainTextBytes);
                Entries[key] = entry;
            }
            else
            {
                Entries.Add(key, Convert.ToBase64String(plainTextBytes));
            }
        }

        /// <summary>
        /// Implemented so that <see cref="GameState"/>s can easily be sorted based on the date they were saved.
        /// </summary>
        public int CompareTo(object obj)
        {
            GameState other = (GameState)obj;
            if (SaveDate > other.SaveDate) return 1;
            if (SaveDate < other.SaveDate) return -1;
            return 0;
        }
    }
}
