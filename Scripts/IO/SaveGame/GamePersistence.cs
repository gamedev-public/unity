using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace JGDT.IO.SaveGame
{
    /// <summary>
    /// A static class to Save and Load a <see cref="GameState"/>.
    /// Has Newtonsoft as a dependency: https://github.com/jilleJr/Newtonsoft.Json-for-Unity
    /// </summary>
    public static class GamePersistence
    {
        public static string SaveLocation = $"{Application.persistentDataPath}/saves";
        public static string FileExtension = "bin";

        /// <summary>
        /// Save the current state of the game.
        /// </summary>
        /// <param name="key">The name of the <see cref="GameState"/> on disk.</param>
        /// <param name="state">The <see cref="GameState"/> to persist.</param>
        /// <param name="overwrite">Whether the code should prevent overwriting existing <see cref="GameState"/> or not. Defaults to true.</param>
        public static void Save(string key, GameState state, bool overwrite = true)
        {
            string filePath = $"{SaveLocation}/{key}.{FileExtension}";
            if (Directory.Exists(SaveLocation) == false) Directory.CreateDirectory(SaveLocation);
            if (overwrite == false)
            {
                if (File.Exists(filePath))
                {
                    throw new IOException($"The file '{key}.{FileExtension}' already exists and cannot be overwritten.");
                }
            }

            using (FileStream fs = File.Open(filePath, FileMode.Create))
            {
                BinaryWriter writer = new BinaryWriter(fs);
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(state));
                writer.Write(Convert.ToBase64String(plainTextBytes));
                writer.Flush();
            }
        }

        /// <summary>
        /// Load a specific state of the game.
        /// </summary>
        /// <param name="key">The name of the save file</param>
        /// <returns>A <see cref="GameState"/> object containing the persisted game state.</returns>
        public static GameState Load(string key)
        {
            if (Directory.Exists(SaveLocation) == false)
            {
                throw new ArgumentException($"No persisted GameState with key '{key}'");
            }

            string filePath = $"{SaveLocation}/{key}.{FileExtension}";
            if (File.Exists(filePath))
            {
                using (FileStream fs = File.Open(filePath, FileMode.Open))
                {
                    BinaryReader reader = new BinaryReader(fs);
                    byte[] encodedBytes = Convert.FromBase64String(reader.ReadString());
                    GameState state = new GameState(JsonConvert.DeserializeObject<GameState>(Encoding.UTF8.GetString(encodedBytes)));
                    return state;
                }
            }
            else
            {
                throw new ArgumentException($"No persisted GameState with key '{key}'");
            }
        }

        /// <summary>
        /// Will remove a Save File from the disk.
        /// </summary>
        /// <param name="key">The key of the save file.</param>
        public static void Remove(string key)
        {
            if (Directory.Exists(SaveLocation) == false) return;

            string path = $"{SaveLocation}/{key}.{FileExtension}";
            if (File.Exists(path)) File.Delete(path);
        }

        /// <summary>
        /// Will remove all Save Files.
        /// </summary>
        public static void RemoveAll()
        {
            if (Directory.Exists(SaveLocation) == false) return;
            foreach (string filePath in Directory.GetFiles(SaveLocation))
            {
                File.Delete(filePath);
            }
        }

        /// <summary>
        /// Used to get a list of all persisted <see cref="GameState"/> files on disk.
        /// </summary>
        /// <returns>A list of <see cref="SaveFileInfo"/> that acts as meta-data about a <see cref="GameState"/></returns>
        public static List<SaveFileInfo> GetAllGameStateFileInfos()
        {
            if (Directory.Exists(SaveLocation) == false) return new List<SaveFileInfo>();
            List<string> paths = new List<string>(Directory.GetFiles(SaveLocation));
            List<SaveFileInfo> infos = new List<SaveFileInfo>();
            foreach (string path in paths)
            {
                string pathSplit = path.Split('\\')[1];
                infos.Add(new SaveFileInfo
                {
                    Path = path,
                    Key = pathSplit.Remove(pathSplit.IndexOf($".{FileExtension}")),
                    SaveDate = File.GetCreationTime(path)
                });
            }
            return infos;
        }
    }

    /// <summary>
    /// A meta-data struct used to describe a persisted <see cref="GameState"/>
    /// </summary>
    public struct SaveFileInfo
    {
        public string Path;
        public string Key;
        public DateTime SaveDate;

        public string ToString(string dateTimeFormat = "")
            => $"[Path:{Path}|Key:{Key}|SaveDate:{SaveDate.ToString(dateTimeFormat)}]";
    }
}