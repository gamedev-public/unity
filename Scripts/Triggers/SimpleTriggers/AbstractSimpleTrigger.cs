using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace JGDT.Triggers.SimpleTriggers
{
    /// <summary>
    /// The parent class for all the simple triggers. This class HAS to be included to use the Simple Trigger classes.
    /// </summary>
    public abstract class AbstractSimpleTrigger : MonoBehaviour
    {
        // Public Fields
        [Tooltip("The tags (if any) that can interact with this trigger. Leave empty for every tag.")]
        public List<string> TriggerTags;
        [Tooltip("Whether the trigger should consider Line of Sight or not.")]
        public bool UsesLineOfSight = false;
        [Tooltip("Whether this trigger will only activate once or every time someone moves into the trigger.")]
        public bool IsOneShot = false;
        [Tooltip("The events to fire when something enters the trigger.")]
        public UnityEvent OnEnter;
        [Tooltip("The events to fire while something is in the trigger.")]
        public UnityEvent OnStay;
        [Tooltip("The events to fire when something exits the trigger.")]
        public UnityEvent OnExit;
        [Header("Debugging")]
        [Tooltip("Whether to show debug information or not.")]
        public bool ShowDebug = false;
        [Space(20), Header("Read Only")]
        // Private Fields
        [SerializeField]
        [Tooltip("The collider that this script needs to function. Do not change manually.")]
        internal Collider _collider;
        [SerializeField]
        [Tooltip("Whether the trigger has triggered or not. Only evaluated if IsOneShot is true.")]
        private bool _hasTriggered = false;

        #region Trigger Methods
        protected void OnTriggerEnter(Collider other) => InvokeEvents(OnEnter, other);
        protected void OnTriggerStay(Collider other) => InvokeEvents(OnStay, other);
        protected void OnTriggerExit(Collider other) => InvokeEvents(OnExit, other);

        private void InvokeEvents(UnityEvent events, Collider other)
        {
            bool triggered = false;
            // Could be a one-shot event. If this is true, then it has already fired.
            if (_hasTriggered == false)
            {
                // If there are no tags, we will react on any collision.
                if (TriggerTags.Count == 0)
                {
                    // If the trigger relies on LineOfSight check it first.
                    if (UsesLineOfSight)
                    {
                        if (HasLineOfSight(other))
                        {
                            triggered = true;
                        }
                    }
                    else
                    {
                        triggered = true;
                    }
                }
                else if (TriggerTags.Contains(other.tag))
                {
                    // If the trigger relies on LineOfSight check it first.
                    if (UsesLineOfSight)
                    {
                        if (HasLineOfSight(other))
                        {
                            triggered = true;
                        }
                    }
                    else
                    {
                        triggered = true;
                    }
                }

                if (triggered == true)
                {
                    if (IsOneShot == true)
                    {
                        _hasTriggered = true;
                    }
                    events.Invoke();
                }
            }
        }

        private bool HasLineOfSight(Collider other)
        {
            Ray ray = new Ray(transform.position, other.transform.position - transform.position);
            if(ShowDebug == true) Debug.DrawLine(ray.origin, ray.direction * float.MaxValue, Color.red, 5f);
            if (Physics.Raycast(ray, out RaycastHit hit, float.MaxValue))
            {
                if (hit.collider.gameObject != other.gameObject)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}