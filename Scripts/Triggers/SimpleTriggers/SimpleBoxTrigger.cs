﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JGDT.Triggers.SimpleTriggers
{
    [RequireComponent(typeof(BoxCollider))]
    public class SimpleBoxTrigger : AbstractSimpleTrigger
    {
        private void OnValidate()
        {
            if (_collider == null)
            {
                _collider = GetComponent<Collider>();
                _collider.enabled = true;
                _collider.isTrigger = true;
            }
        }
    }
}
