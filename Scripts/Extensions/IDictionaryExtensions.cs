﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JGDT.Extensions
{
    /// <summary>
    /// A utility class that adds useful extensions for any <see cref="IDictionary{T, V}"/>.
    /// </summary>
    public static class IDictionaryExtensions
    {
        /// <summary>
        /// Swap two Values around in an <see cref="IDictionary{T, V}"/>.
        /// </summary>
        /// <typeparam name="T">The type of Key in the <see cref="IDictionary{T, V}"/>.</typeparam>
        /// <typeparam name="V">The type of Value in the <see cref="IDictionary{T, V}"/>.</typeparam>
        /// <param name="dict">The <see cref="IDictionary{T, V}"/> that needs to have Values swapped.</param>
        /// <param name="key1">The Key of the first item in the <see cref="IDictionary{T, V}"/> to swap.</param>
        /// <param name="key2">The Key of the second item in the <see cref="IDictionary{T, V}"/> to swap.</param>
        public static void Swap<T, V>(this IDictionary<T, V> dict, T key1, T key2)
        {
            if (!dict.ContainsKey(key1) || !dict.ContainsKey(key2))
                throw new InvalidOperationException("Dictiohnary does not contain one or either of the provided keys.");
            V v = dict[key1];
            V v2 = dict[key2];
            dict[key1] = v2;
            dict[key2] = v;
        }

        /// <summary>
        /// Shuffle an <see cref="IDictionary{T, V}"/> using Fisher-Yates Shuffle.
        /// </summary>
        /// <typeparam name="T">The type of Key in the <see cref="IDictionary{T, V}"/>.</typeparam>
        /// <typeparam name="V">The type of Value in the <see cref="IDictionary{T, V}"/>.</typeparam>
        /// <param name="dict">The <see cref="IDictionary{T, V}"/> that needs to have Values Shuffled.</param>
        public static void Shuffle<T, V>(this IDictionary<T, V> dict)
        {
            for (int index = 0; index < dict.Keys.Count; index++)
            {
                int rnd = UnityEngine.Random.Range(index, dict.Keys.Count);
                T randomKey = dict.Keys.ElementAt(rnd);
                T indexKey = dict.Keys.ElementAt(index);
                dict.Swap(randomKey, indexKey);
            }
        }

        /// <summary>
        /// Shuffle an <see cref="IDictionary{T, V}"/> using Fisher-Yates Shuffle with a seed.
        /// </summary>
        /// <typeparam name="T">The type of Key in the <see cref="IDictionary{T, V}"/>.</typeparam>
        /// <typeparam name="V">The type of Value in the <see cref="IDictionary{T, V}"/>.</typeparam>
        /// <param name="dict">The <see cref="IDictionary{T, V}"/> that needs to have Values Shuffled.</param>
        /// <param name="seed">The seed for this Shuffle.</param>
        public static void Shuffle<T, V>(this IDictionary<T, V> dict, int seed)
        {
            UnityEngine.Random.InitState(seed);
            for (int index = 0; index < dict.Keys.Count; index++)
            {
                int rnd = UnityEngine.Random.Range(index, dict.Keys.Count);
                T randomKey = dict.Keys.ElementAt(rnd);
                T indexKey = dict.Keys.ElementAt(index);
                dict.Swap(randomKey, indexKey);
            }
        }
    }
}
