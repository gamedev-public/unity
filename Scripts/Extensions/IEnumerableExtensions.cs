﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JGDT.Extensions
{
    /// <summary>
    /// A utility class that adds useful extensions for any <see cref="IEnumerable{T}"/>.
    /// </summary>
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Get a random element from an <see cref="IEnumerable{T}"/>.
        /// </summary>
        /// <typeparam name="T">The type of object in the <see cref="IEnumerable{T}"/></typeparam>
        /// <param name="collection">The <see cref="IEnumerable{T}"/> to get an element from.</param>
        /// <returns>A random element from the <see cref="IEnumerable{T}"/></returns>
        public static T RandomElement<T>(this IEnumerable<T> collection)
        {
            int index = UnityEngine.Random.Range(0, collection.Count());
            return collection.ElementAt(index);
        }
    }
}