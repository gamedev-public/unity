/*
 * MIT License
 * 
 * Copyright (c) 2021 JustGameDevThings
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace JGDT.Extensions
{
    /// <summary>
    /// A utility class that adds useful extensions for any <see cref="Array"/>.
    /// </summary>
    public static class ArrayExtensions
    {
        #region Single-Dimensional Arrays
        /// <summary>
        /// Get a random element from an <see cref="Array"/>.
        /// </summary>
        /// <typeparam name="T">The type of object in the <see cref="Array"/></typeparam>
        /// <param name="arr">The <see cref="Array"/> to get an element from.</param>
        /// <returns>A random element from the <see cref="Array"/></returns>
        public static T RandomElement<T>(this T[] arr)
        {
            int index = UnityEngine.Random.Range(0, arr.Length);
            return arr[index];
        }
        #endregion

        #region Multi-Dimesional Arrays
        /// <summary>
        /// Get a random element from a multi-dimensional <see cref="Array"/>.
        /// </summary>
        /// <typeparam name="T">The type of object in the multi-dimnensional <see cref="Array"/></typeparam>
        /// <param name="arr">The multi-dimensional <see cref="Array"/> to get an element from.</param>
        /// <returns>A random element from the multi-dimensional <see cref="Array"/></returns>
        public static T RandomElement<T>(this T[,] arr)
        {
            int randomX = UnityEngine.Random.Range(0, arr.GetLength(0));
            int randomY = UnityEngine.Random.Range(0, arr.GetLength(1));
            return arr[randomX, randomY];
        }

        /// <summary>
        /// See if any element is in the multidimensional array at all. Similar to Linq.Any.
        /// </summary>
        /// <typeparam name="T">The type of object in the multi-dimensional <see cref="Array"/></typeparam>
        /// <param name="arr">The multi-dimensional <see cref="Array"/> to check the condition in.</param>
        /// <returns>True if any object exists in the enumerator otherwise false.</returns>
        public static bool Any<T>(this T[,] arr)
        {
            if (arr == null) throw new ArgumentNullException("array");
            if (arr.GetEnumerator().MoveNext()) return true;
            return false;
        }

        /// <summary>
        /// See if any element in the multidimensional array satisfy the given condition. Similar to Linq.Any(IEnumerator, Func).
        /// </summary>
        /// <typeparam name="T">The type of object in the multi-dimensional <see cref="Array"/></typeparam>
        /// <param name="arr">The multi-dimensional <see cref="Array"/> to check the condition in.</param>
        /// <returns>True if any object exists that satisfies the conditions otherwise false.</returns>
        public static bool Any<T>(this T[,] arr, Func<T, bool> conditions)
        {
            if (arr == null) throw new ArgumentNullException("array");
            if (conditions == null) throw new ArgumentNullException("conditions");
            foreach (T element in arr)
            {
                if (conditions(element)) return true;
            }
            return false;
        }

        /// <summary>
        /// See if all elements in the multidimensional array satisfy the given condition. Similar to Linq.All(IEnumerator, Func).
        /// </summary>
        /// <typeparam name="T">The type of object in the multi-dimensional <see cref="Array"/></typeparam>
        /// <param name="arr">The multi-dimensional <see cref="Array"/> to check the condition in.</param>
        /// <returns>True if All objects satisfies the conditions otherwise false.</returns>
        public static bool All<T>(this T[,] arr, Func<T, bool> conditions)
        {
            if (arr == null) throw new ArgumentNullException("array");
            if (conditions == null) throw new ArgumentNullException("conditions");
            foreach (T element in arr)
            {
                if (!conditions(element)) return false;
            }
            return true;
        }

        /// <summary>
        /// Get all elements that are in both multi-dimensional <see cref="Array"/>. Assumes that both arrays are the same size.
        /// </summary>
        /// <typeparam name="T">The type of object in the multi-dimensional <see cref="Array"/></typeparam>
        /// <param name="a">The first <see cref="Array"/></param>
        /// <param name="b">The second <see cref="Array"/></param>
        /// <returns>A one-dimensional <see cref="Array"/> containing all the <see cref="{T}"/> found in both.</returns>
        public static T[] Intersect<T>(this T[,] a, T[,] b)
        {
            if (a.Length == b.Length)
            {
                List<T> list = new List<T>();
                for (int x = 0; x < a.GetLength(0); x++)
                {
                    for (int y = 0; y < a.GetLength(1); y++)
                    {
                        if (a[x, y].Equals(b[x, y]))
                        {
                            list.Add(a[x, y]);
                        }
                    }
                }
                return list.ToArray();
            }
            else
            {
                throw new InvalidOperationException("The arrays are not equal size.");
            }
        }

        /// <summary>
        /// Will flatten a multi-dimensional <see cref="Array"/>.
        /// </summary>
        /// <typeparam name="T">The type of object in the array.</typeparam>
        /// <param name="multidimensional">The <see cref="Array"/> to turn into a flat <see cref="Array"/></param>
        /// <returns>A flattened <see cref="Array"/>.</returns>
        public static T[] Flatten<T>(this T[,] multidimensional)
        {
            int length = multidimensional.GetLength(0) * multidimensional.GetLength(1);
            T[] result = new T[length];
            int index = 0;
            for (int x = 0; x < multidimensional.GetLength(0); x++)
            {
                for (int y = 0; y < multidimensional.GetLength(1); y++)
                {
                    result[index] = multidimensional[x, y];
                    index++;
                }
            }
            return result;
        }
		
        /// <summary>
        /// Will mirror a 2D array Horizontally. The array is assumed to have the same number of rows and columns.
        /// </summary>
        /// <typeparam name="T">The type of object int he array.</typeparam>
        /// <param name="array">The <see cref="Array"/> to mirror.</param>
        public static T[,] MirrorHorizontally<T>(T[,] array)
        {
            int height = array.GetLength(0);
            int width = array.GetLength(1);

            T[,] mirroredArray = new T[height, width];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    mirroredArray[i, j] = array[i, width - 1 - j];
                }
            }
           return mirroredArray;
        }

        /// <summary>
        /// Will mirror a 2D array Vertically. The array is assumed to have the same number of rows and columns.
        /// </summary>
        /// <typeparam name="T">The type of object int he array.</typeparam>
        /// <param name="array">The <see cref="Array"/> to mirror.</param>
        public static T[,] MirrorVertically<T>(T[,] array)
        {
            int height = array.GetLength(0);
            int width = array.GetLength(1);

            T[,] mirroredArray = new T[height, width];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    mirroredArray[i, j] = array[height - 1 - i, j];
                }
            }
            return mirroredArray;
        }


        /// <summary>
        /// Will rotate an array 90 degrees Clockwise. It is assumed that the array is rectangular.
        /// </summary>
        /// <typeparam name="T">The type of object int he array.</typeparam>
        /// <param name="array">The <see cref="Array"/> to rotate.</param>
        public static T[,] RotateClockwise<T>(T[,] array)
        {
            int height = array.GetLength(0);
            int width = array.GetLength(1);

            T[,] rotatedArray = new T[width, height];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    rotatedArray[j, height - 1 - i] = array[i, j];
                }
            }
            return rotatedArray;
        }

        /// <summary>
        /// Shuffle a multi-dimensional <see cref="Array"/> using Fisher-Yates Shuffle.
        /// </summary>
        /// <typeparam name="T">The type of element in the multi-dimensional <see cref="Array"/>.</typeparam>
        /// <param name="arr">The multi-dimensional <see cref="Array"/> to shuffle.</param>
        public static void Shuffle<T>(this T[,] arr)
        {
            if (arr.GetLength(0) == 0 || arr.GetLength(1) == 0)
                throw new InvalidOperationException("The length of the multi-dimensional array is 0 in one of its dimensions.");
            T[] flat = arr.Flatten();
            flat.Shuffle();
            int index = 0;
            for (int x = 0; x < arr.GetLength(0); x++)
            {
                for (int y = 0; y < arr.GetLength(1); y++)
                {
                    arr[x, y] = flat[index];
                    index++;
                }
            }
        }

        /// <summary>
        /// Shuffle a multi-dimensional <see cref="Array"/> using Fisher-Yates Shuffle with a seed.
        /// </summary>
        /// <typeparam name="T">The type of element in the <see cref="Array"/>.</typeparam>
        /// <param name="arr">The multi-dimensional <see cref="Array"/> to shuffle.</param>
        /// <param name="seed">The seed for this Shuffle.</param>
        public static void Shuffle<T>(this T[,] arr, int seed)
        {
            if (arr.GetLength(0) == 0 || arr.GetLength(1) == 0)
                throw new InvalidOperationException("The length of the multi-dimensional array is 0 in one of its dimensions.");
            T[] flat = arr.Flatten();
            flat.Shuffle(seed);
            int index = 0;
            for (int x = 0; x < arr.GetLength(0); x++)
            {
                for (int y = 0; y < arr.GetLength(1); y++)
                {
                    arr[x, y] = flat[index];
                    index++;
                }
            }
        }
        #endregion
    }
}
