﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JGDT.Extensions
{
    /// <summary>
    /// A utility class to add some Math convenience methods.
    /// </summary>
    public static class MathExtensions
    {
        /// <summary>
        /// Utility function to normalize any float to a 0.0-1.0 range.
        /// </summary>
        /// <param name="value">The value to normalize.</param>
        /// <param name="min">The lowest number in the range.</param>
        /// <param name="max">The highest number in the range.</param>
        /// <returns>The value normalized to a number between 0.0 and 1.0</returns>
        public static float Normalize(float value, float min, float max) => (value - min) / (max - min);

        /// <summary>
        /// Utility function to map a value from one range onto another.
        /// </summary>
        /// <param name="value">The value to map.</param>
        /// <param name="min">The minimum of the value's current range.</param>
        /// <param name="max">The maximum of the value's current range.</param>
        /// <param name="targetMin">The minimum of the value's target range.</param>
        /// <param name="targetMax">The maximum of the value's target range.</param>
        /// <returns>The value mapped onto the target range.</returns>
        public static float MapValueToNewScale(float value, float min, float max, float targetMin, float targetMax)
            => (value - min) * ((targetMax - targetMin) / (max - min)) + targetMin;

        #region Integers
        /// <summary>
        /// Will return the Absolute Sum of all integers found in the <see cref="ICollection{int}"/>.
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{int}"/> to sum.</param>
        /// <returns>The absolute sum of all integers in the <see cref="ICollection{int}"/>.</returns>
        public static int Abs(this ICollection<int> icol)
        {
            int total = 0;
            foreach (int number in icol)
            {
                total += Math.Abs(number);
            }
            return total;
        }

        /// <summary>
        /// Will return the average value of the <see cref="ICollection{int}"/>.
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{int}"/> to average.</param>
        /// <param name="roundingUp">Whether to Round Up or Down.</param>
        /// <returns>The average value in the <see cref="ICollection{int}"/> as an integer.</returns>
        public static int Average(this ICollection<int> icol, bool roundingUp = true)
        {
            if (icol.Count == 0) throw new InvalidOperationException("Can't Average an empty Collection.");
            float total = icol.Sum() / icol.Count;
            return (int)(roundingUp == true ? Math.Ceiling(total) : Math.Floor(total)); ;
        }

        /// <summary>
        /// Will return the median value of the <see cref="ICollection{int}"/>
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{int}"/> to get the median from.</param>
        /// <returns>The median value in the <see cref="ICollection{int}"/>.</returns>
        public static int Median(this ICollection<int> icol)
        {
            if (icol.Count == 0) throw new InvalidOperationException("Cannot get Median of empty collection.");
            if (icol.Count == 1) return icol.First();

            List<int> list = (List<int>)icol;
            list.Sort();
            if (list.Count % 2 == 1)
            {
                // odd
                return list[(list.Count / 2) + 1];
            }
            else
            {
                // even
                int index = list.Count / 2;
                return (list[index] + list[index + 1]) / 2;
            }
        }

        /// <summary>
        /// Will return the mode value(s) of the <see cref="ICollection{int}"/>
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{int}"/> to get the mode(s) from.</param>
        /// <returns>An <see cref="ICollection{int}"/> of modes found.</returns>
        public static ICollection<int> Mode(this ICollection<int> icol)
        {
            if (icol.Count == 0) throw new InvalidOperationException("Cannot get Mode of empty collection.");
            if (icol.Count == 1) return icol;

            Dictionary<int, int> dict = new Dictionary<int, int>();
            int highestOccurrence = 0;
            foreach (int number in icol)
            {
                if (!dict.ContainsKey(number)) dict.Add(number, 0);
                dict[number] += 1;
                if (dict[number] > highestOccurrence) highestOccurrence = dict[number];
            }

            ICollection<int> modes = new List<int>();
            foreach (KeyValuePair<int, int> pair in dict)
            {
                if (pair.Value == highestOccurrence)
                {
                    modes.Add(pair.Key);
                }
            }
            return modes;
        }
        #endregion

        #region Floats
        /// <summary>
        /// Will return the Absolute Sum of all floats found in the <see cref="ICollection{float}"/>.
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{float}"/> to sum.</param>
        /// <returns>The absolute sum of all float in the <see cref="ICollection{float}"/>.</returns>
        public static float Abs(this IList<float> icol)
        {
            float total = 0;
            foreach (float number in icol)
            {
                total += Math.Abs(number);
            }
            return total;
        }

        /// <summary>
        /// Will return the average value of the <see cref="ICollection{float}"/>.
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{float}"/> to average.</param>
        /// <returns>The average value in the <see cref="ICollection{float}"/> as a float.</returns>
        public static float Average(this ICollection<float> icol)
        {
            if (icol.Count == 0) throw new InvalidOperationException("Can't Average an empty Collection.");
            return icol.Sum() / icol.Count;
        }

        /// <summary>
        /// Will return the median value of the <see cref="ICollection{float}"/>
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{float}"/> to get the median from.</param>
        /// <returns>The median value in the <see cref="ICollection{float}"/>.</returns>
        public static float Median(this ICollection<float> icol)
        {
            if (icol.Count == 0) throw new InvalidOperationException("Cannot get Median of empty collection.");
            if (icol.Count == 1) return icol.First();

            List<float> list = (List<float>)icol;
            list.Sort();
            if (list.Count % 2 == 1)
            {
                // odd
                return list[(list.Count / 2) + 1];
            }
            else
            {
                // even
                int index = list.Count / 2;
                return (list[index] + list[index + 1]) / 2;
            }
        }

        /// <summary>
        /// Will return the mode value(s) of the <see cref="ICollection{float}"/>
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{float}"/> to get the mode(s) from.</param>
        /// <returns>An <see cref="ICollection{float}"/> of modes found.</returns>
        public static ICollection<float> Mode(this ICollection<float> icol)
        {
            if (icol.Count == 0) throw new InvalidOperationException("Cannot get Mode of empty collection.");
            if (icol.Count == 1) return icol;

            Dictionary<float, int> dict = new Dictionary<float, int>();
            int highestOccurrence = 0;
            foreach (float number in icol)
            {
                if (!dict.ContainsKey(number)) dict.Add(number, 0);
                dict[number] += 1;
                if (dict[number] > highestOccurrence) highestOccurrence = dict[number];
            }

            ICollection<float> modes = new List<float>();
            foreach (KeyValuePair<float, int> pair in dict)
            {
                if (pair.Value == highestOccurrence)
                {
                    modes.Add(pair.Key);
                }
            }
            return modes;
        }
        #endregion

        #region Doubles
        /// <summary>
        /// Will return the Absolute Sum of all doubles found in the <see cref="ICollection{double}"/>.
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{double}"/> to sum.</param>
        /// <returns>The absolute sum of all doubles in the <see cref="ICollection{double}"/>.</returns>
        public static double Abs(this ICollection<double> icol)
        {
            double total = 0;
            foreach (double number in icol)
            {
                total += Math.Abs(number);
            }
            return total;
        }

        /// <summary>
        /// Will return the average value of the <see cref="ICollection{double}"/>.
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{double}"/> to average.</param>
        /// <returns>The average value in the <see cref="ICollection{double}"/> as a double.</returns>
        public static double Average(this ICollection<double> icol)
        {
            if (icol.Count == 0) throw new InvalidOperationException("Can't Average an empty Collection.");
            return icol.Sum() / icol.Count;
        }

        /// <summary>
        /// Will return the median value of the <see cref="ICollection{double}"/>
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{double}"/> to get the median from.</param>
        /// <returns>The median value in the <see cref="ICollection{double}"/>.</returns>
        public static double Median(this ICollection<double> icol)
        {
            if (icol.Count == 0) throw new InvalidOperationException("Cannot get Median of empty collection.");
            if (icol.Count == 1) return icol.First();

            List<double> list = (List<double>)icol;
            list.Sort();
            if (list.Count % 2 == 1)
            {
                // odd
                return list[(list.Count / 2) + 1];
            }
            else
            {
                // even
                int index = list.Count / 2;
                return (list[index] + list[index + 1]) / 2;
            }
        }

        /// <summary>
        /// Will return the mode value(s) of the <see cref="ICollection{double}"/>
        /// </summary>
        /// <param name="icol">The <see cref="ICollection{double}"/> to get the mode(s) from.</param>
        /// <returns>An <see cref="ICollection{double}"/> of modes found.</returns>
        public static ICollection<double> Mode(this ICollection<double> icol)
        {
            if (icol.Count == 0) throw new InvalidOperationException("Cannot get Mode of empty collection.");
            if (icol.Count == 1) return icol;

            Dictionary<double, int> dict = new Dictionary<double, int>();
            int highestOccurrence = 0;
            foreach (float number in icol)
            {
                if (!dict.ContainsKey(number)) dict.Add(number, 0);
                dict[number] += 1;
                if (dict[number] > highestOccurrence) highestOccurrence = dict[number];
            }

            ICollection<double> modes = new List<double>();
            foreach (KeyValuePair<double, int> pair in dict)
            {
                if (pair.Value == highestOccurrence)
                {
                    modes.Add(pair.Key);
                }
            }
            return modes;
        }
        #endregion
    }
}
