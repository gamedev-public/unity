﻿using UnityEngine;
using static JGDT.Extensions.Easings.EasingUtility;

namespace JGDT.Extensions.Easings
{
    /// <summary>
    /// A utility class based on the <see cref="EasingUtility"/> class to get <see cref="Vector2"/> easing functions. See usage example in class documentation.
    /// </summary>
    /*
     * How to use this class:
     * This class is made to retrieve and cache an easing function for use later. Good use-cases include coroutines or other update-dependent loops.
     * First store an Easing Function found in the EasingUtility class, for example like this:
     * 
     * Function easingFunction = EasingUtility.GetEasingFunction(Ease.EaseInOutQuad);
     * 
     * Then you can make use of this class like so:
     * Vector2Easings.Interpolate(easingFunction, start, end, alpha)
     * 
     * The benefit here is that an easing function is cached and reused until you no longer need it, then you toss it.
     * You could gain more performance if you store all the easing functions you need to use ahead of time and then just
     * use them whenever you need to.
     * 
     * You can also use the provided easing functions in this class, however will be slower due to no function caching.
     */
    public static class Vector2Easings
    {
        /// <summary>
        /// Used to get an interpolated <see cref="Vector2"/> from a given type of easing.
        /// </summary>
        /// <param name="easingFunction">The function to use to interpolate.</param>
        /// <param name="start">The <see cref="Vector2"/> where the entity starts.</param>
        /// <param name="end">The <see cref="Vector2"/> where the entity is supposed to end up.</param>
        /// <param name="alpha">A value between 0 and 1 representing where in the easing the function is currently evaluating.</param>
        /// <returns>An interpolated <see cref="Vector2"/> based on the given <see cref="Function"/>.</returns>
        public static Vector2 Interpolate(Function easingFunction, Vector2 start, Vector2 end, float alpha)
            => new Vector2(
                easingFunction(start.x, end.x, alpha),
                easingFunction(start.y, end.y, alpha));

        public static Vector2 Linear(Vector2 start, Vector2 end, float alpha)           => Interpolate(GetEasingFunction(Ease.Linear), start, end, alpha);
        public static Vector2 Spring(Vector2 start, Vector2 end, float alpha)           => Interpolate(GetEasingFunction(Ease.Spring), start, end, alpha);
        public static Vector2 EaseInQuad(Vector2 start, Vector2 end, float alpha)       => Interpolate(GetEasingFunction(Ease.EaseInQuad), start, end, alpha);
        public static Vector2 EaseOutQuad(Vector2 start, Vector2 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseOutQuad), start, end, alpha);
        public static Vector2 EaseInOutQuad(Vector2 start, Vector2 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInOutQuad), start, end, alpha);
        public static Vector2 EaseInCubic(Vector2 start, Vector2 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseInCubic), start, end, alpha);
        public static Vector2 EaseOutCubic(Vector2 start, Vector2 end, float alpha)     => Interpolate(GetEasingFunction(Ease.EaseOutCubic), start, end, alpha);
        public static Vector2 EaseInOutCubic(Vector2 start, Vector2 end, float alpha)   => Interpolate(GetEasingFunction(Ease.EaseInOutCubic), start, end, alpha);
        public static Vector2 EaseInQuart(Vector2 start, Vector2 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseInQuart), start, end, alpha);
        public static Vector2 EaseOutQuart(Vector2 start, Vector2 end, float alpha)     => Interpolate(GetEasingFunction(Ease.EaseOutQuart), start, end, alpha);
        public static Vector2 EaseInOutQuart(Vector2 start, Vector2 end, float alpha)   => Interpolate(GetEasingFunction(Ease.EaseInOutQuart), start, end, alpha);
        public static Vector2 EaseInQuint(Vector2 start, Vector2 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseInQuint), start, end, alpha);
        public static Vector2 EaseOutQuint(Vector2 start, Vector2 end, float alpha)     => Interpolate(GetEasingFunction(Ease.EaseOutQuint), start, end, alpha);
        public static Vector2 EaseInOutQuint(Vector2 start, Vector2 end, float alpha)   => Interpolate(GetEasingFunction(Ease.EaseInOutQuint), start, end, alpha);
        public static Vector2 EaseInSine(Vector2 start, Vector2 end, float alpha)       => Interpolate(GetEasingFunction(Ease.EaseInSine), start, end, alpha);
        public static Vector2 EaseOutSine(Vector2 start, Vector2 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseOutSine), start, end, alpha);
        public static Vector2 EaseInOutSine(Vector2 start, Vector2 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInOutSine), start, end, alpha);
        public static Vector2 EaseInExpo(Vector2 start, Vector2 end, float alpha)       => Interpolate(GetEasingFunction(Ease.EaseInExpo), start, end, alpha);
        public static Vector2 EaseOutExpo(Vector2 start, Vector2 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseOutExpo), start, end, alpha);
        public static Vector2 EaseInOutExpo(Vector2 start, Vector2 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInOutExpo), start, end, alpha);
        public static Vector2 EaseInCirc(Vector2 start, Vector2 end, float alpha)       => Interpolate(GetEasingFunction(Ease.EaseInCirc), start, end, alpha);
        public static Vector2 EaseOutCirc(Vector2 start, Vector2 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseOutCirc), start, end, alpha);
        public static Vector2 EaseInOutCirc(Vector2 start, Vector2 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInOutCirc), start, end, alpha);
        public static Vector2 EaseInBounce(Vector2 start, Vector2 end, float alpha)     => Interpolate(GetEasingFunction(Ease.EaseInBounce), start, end, alpha);
        public static Vector2 EaseOutBounce(Vector2 start, Vector2 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseOutBounce), start, end, alpha);
        public static Vector2 EaseInOutBounce(Vector2 start, Vector2 end, float alpha)  => Interpolate(GetEasingFunction(Ease.EaseInOutBounce), start, end, alpha);
        public static Vector2 EaseInBack(Vector2 start, Vector2 end, float alpha)       => Interpolate(GetEasingFunction(Ease.EaseInBack), start, end, alpha);
        public static Vector2 EaseOutBack(Vector2 start, Vector2 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseOutBack), start, end, alpha);
        public static Vector2 EaseInOutBack(Vector2 start, Vector2 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInOutBack), start, end, alpha);
        public static Vector2 EaseInElastic(Vector2 start, Vector2 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInElastic), start, end, alpha);
        public static Vector2 EaseOutElastic(Vector2 start, Vector2 end, float alpha)   => Interpolate(GetEasingFunction(Ease.EaseOutElastic), start, end, alpha);
        public static Vector2 EaseInOutElastic(Vector2 start, Vector2 end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutElastic), start, end, alpha);
    }
}
