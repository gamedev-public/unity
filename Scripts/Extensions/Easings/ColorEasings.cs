using UnityEngine;

namespace JGDT.Extensions.Easings
{
    /// <summary>
    /// A utility class based on the <see cref="EasingUtility"/> class to get <see cref="Color"/> easing functions. See usage example in class documentation.
    /// </summary>
    /*
     * How to use this class:
     * This class is made to retrieve and cache an easing function for use later. Good use-cases include coroutines or other update-dependent loops.
     * First store an Easing Function found in the EasingUtility class, for example like this:
     * 
     * Function easingFunction = EasingUtility.GetEasingFunction(Ease.EaseInOutQuad);
     * 
     * Then you can make use of this class like so:
     * ColorEasings.GetComputedColor(easingFunction, start, end, alpha)
     * 
     * The benefit here is that an easing function is cached and reused until you no longer need it, then you toss it.
     * You could gain more performance if you store all the easing functions you need to use ahead of time and then just
     * use them whenever you need to.
     * 
     * You can also use the provided easing functions in this class, however will be slower due to no function caching.
     */
    public static class ColorEasings
    {
        /// <summary>
        /// Used to get an interpolated <see cref="Color"/> from a given type of easing.
        /// </summary>
        /// <param name="easingFunction">The function to use to interpolate.</param>
        /// <param name="start">The <see cref="Color"/> to start interoplating from.</param>
        /// <param name="end">The <see cref="Color"/> to end interpolating to.</param>
        /// <param name="alpha">A value between 0 and 1 representing where in the easing the function is currently evaluating.</param>
        /// <returns>An interpolated <see cref="Color"/> based on the given <see cref="Function"/>.</returns>
        public static Color Interpolate(Function easingFunction, Color start, Color end, float alpha)
            => new Color(
                easingFunction(start.r, end.r, alpha),
                easingFunction(start.g, end.g, alpha),
                easingFunction(start.b, end.b, alpha),
                easingFunction(start.a, end.a, alpha));

        public static Color Linear(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.Linear), start, end, alpha);
        public static Color Spring(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.Spring), start, end, alpha);
        public static Color EaseInQuad(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInQuad), start, end, alpha);
        public static Color EaseOutQuad(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutQuad), start, end, alpha);
        public static Color EaseInOutQuad(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutQuad), start, end, alpha);
        public static Color EaseInCubic(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInCubic), start, end, alpha);
        public static Color EaseOutCubic(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutCubic), start, end, alpha);
        public static Color EaseInOutCubic(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutCubic), start, end, alpha);
        public static Color EaseInQuart(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInQuart), start, end, alpha);
        public static Color EaseOutQuart(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutQuart), start, end, alpha);
        public static Color EaseInOutQuart(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutQuart), start, end, alpha);
        public static Color EaseInQuint(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInQuint), start, end, alpha);
        public static Color EaseOutQuint(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutQuint), start, end, alpha);
        public static Color EaseInOutQuint(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutQuint), start, end, alpha);
        public static Color EaseInSine(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInSine), start, end, alpha);
        public static Color EaseOutSine(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutSine), start, end, alpha);
        public static Color EaseInOutSine(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutSine), start, end, alpha);
        public static Color EaseInExpo(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInExpo), start, end, alpha);
        public static Color EaseOutExpo(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutExpo), start, end, alpha);
        public static Color EaseInOutExpo(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutExpo), start, end, alpha);
        public static Color EaseInCirc(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInCirc), start, end, alpha);
        public static Color EaseOutCirc(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutCirc), start, end, alpha);
        public static Color EaseInOutCirc(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutCirc), start, end, alpha);
        public static Color EaseInBounce(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInBounce), start, end, alpha);
        public static Color EaseOutBounce(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutBounce), start, end, alpha);
        public static Color EaseInOutBounce(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutBounce), start, end, alpha);
        public static Color EaseInBack(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInBack), start, end, alpha);
        public static Color EaseOutBack(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutBack), start, end, alpha);
        public static Color EaseInOutBack(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutBack), start, end, alpha);
        public static Color EaseInElastic(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInElastic), start, end, alpha);
        public static Color EaseOutElastic(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutElastic), start, end, alpha);
        public static Color EaseInOutElastic(Color start, Color end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutElastic), start, end, alpha);
    }
}
