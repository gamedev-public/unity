using UnityEngine;
using static JGDT.Extensions.Easings.EasingUtility;

namespace JGDT.Extensions.Easings
{
    /// <summary>
    /// A utility class based on the <see cref="EasingUtility"/> class to get <see cref="Vector3"/> easing functions. See usage example in class documentation.
    /// </summary>
    /*
     * How to use this class:
     * This class is made to retrieve and cache an easing function for use later. Good use-cases include coroutines or other update-dependent loops.
     * First store an Easing Function found in the EasingUtility class, for example like this:
     * 
     * Function easingFunction = EasingUtility.GetEasingFunction(Ease.EaseInOutQuad);
     * 
     * Then you can make use of this class like so:
     * Vector3Easings.Interpolate(easingFunction, start, end, alpha)
     * 
     * The benefit here is that an easing function is cached and reused until you no longer need it, then you toss it.
     * You could gain slightly more performance if you store all the easing functions you need to use ahead of time 
     * (if that is possible for you) and then just use them whenever you need to.
     * 
     * You can also use the provided easing functions in this class, however will be slower due to no function caching.
     */
    public static class Vector3Easings
    {
        /// <summary>
        /// Used to get an interpolated <see cref="Vector3"/> from a given type of easing.
        /// </summary>
        /// <param name="easingFunction">The function to use to interpolate.</param>
        /// <param name="start">The <see cref="Vector3"/> where the entity starts.</param>
        /// <param name="end">The <see cref="Vector3"/> where the entity is supposed to end up.</param>
        /// <param name="alpha">A value between 0 and 1 representing where in the easing the function is currently evaluating.</param>
        /// <returns>An interpolated <see cref="Vector3"/> based on the given <see cref="Function"/>.</returns>
        public static Vector3 Interpolate(Function easingFunction, Vector3 start, Vector3 end, float alpha)
            => new Vector3(
                easingFunction(start.x, end.x, alpha),
                easingFunction(start.y, end.y, alpha),
                easingFunction(start.z, end.z, alpha));

        public static Vector3 Linear(Vector3 start, Vector3 end, float alpha)           => Interpolate(GetEasingFunction(Ease.Linear), start, end, alpha);
        public static Vector3 Spring(Vector3 start, Vector3 end, float alpha)           => Interpolate(GetEasingFunction(Ease.Spring), start, end, alpha);
        public static Vector3 EaseInQuad(Vector3 start, Vector3 end, float alpha)       => Interpolate(GetEasingFunction(Ease.EaseInQuad), start, end, alpha);
        public static Vector3 EaseOutQuad(Vector3 start, Vector3 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseOutQuad), start, end, alpha);
        public static Vector3 EaseInOutQuad(Vector3 start, Vector3 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInOutQuad), start, end, alpha);
        public static Vector3 EaseInCubic(Vector3 start, Vector3 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseInCubic), start, end, alpha);
        public static Vector3 EaseOutCubic(Vector3 start, Vector3 end, float alpha)     => Interpolate(GetEasingFunction(Ease.EaseOutCubic), start, end, alpha);
        public static Vector3 EaseInOutCubic(Vector3 start, Vector3 end, float alpha)   => Interpolate(GetEasingFunction(Ease.EaseInOutCubic), start, end, alpha);
        public static Vector3 EaseInQuart(Vector3 start, Vector3 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseInQuart), start, end, alpha);
        public static Vector3 EaseOutQuart(Vector3 start, Vector3 end, float alpha)     => Interpolate(GetEasingFunction(Ease.EaseOutQuart), start, end, alpha);
        public static Vector3 EaseInOutQuart(Vector3 start, Vector3 end, float alpha)   => Interpolate(GetEasingFunction(Ease.EaseInOutQuart), start, end, alpha);
        public static Vector3 EaseInQuint(Vector3 start, Vector3 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseInQuint), start, end, alpha);
        public static Vector3 EaseOutQuint(Vector3 start, Vector3 end, float alpha)     => Interpolate(GetEasingFunction(Ease.EaseOutQuint), start, end, alpha);
        public static Vector3 EaseInOutQuint(Vector3 start, Vector3 end, float alpha)   => Interpolate(GetEasingFunction(Ease.EaseInOutQuint), start, end, alpha);
        public static Vector3 EaseInSine(Vector3 start, Vector3 end, float alpha)       => Interpolate(GetEasingFunction(Ease.EaseInSine), start, end, alpha);
        public static Vector3 EaseOutSine(Vector3 start, Vector3 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseOutSine), start, end, alpha);
        public static Vector3 EaseInOutSine(Vector3 start, Vector3 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInOutSine), start, end, alpha);
        public static Vector3 EaseInExpo(Vector3 start, Vector3 end, float alpha)       => Interpolate(GetEasingFunction(Ease.EaseInExpo), start, end, alpha);
        public static Vector3 EaseOutExpo(Vector3 start, Vector3 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseOutExpo), start, end, alpha);
        public static Vector3 EaseInOutExpo(Vector3 start, Vector3 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInOutExpo), start, end, alpha);
        public static Vector3 EaseInCirc(Vector3 start, Vector3 end, float alpha)       => Interpolate(GetEasingFunction(Ease.EaseInCirc), start, end, alpha);
        public static Vector3 EaseOutCirc(Vector3 start, Vector3 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseOutCirc), start, end, alpha);
        public static Vector3 EaseInOutCirc(Vector3 start, Vector3 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInOutCirc), start, end, alpha);
        public static Vector3 EaseInBounce(Vector3 start, Vector3 end, float alpha)     => Interpolate(GetEasingFunction(Ease.EaseInBounce), start, end, alpha);
        public static Vector3 EaseOutBounce(Vector3 start, Vector3 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseOutBounce), start, end, alpha);
        public static Vector3 EaseInOutBounce(Vector3 start, Vector3 end, float alpha)  => Interpolate(GetEasingFunction(Ease.EaseInOutBounce), start, end, alpha);
        public static Vector3 EaseInBack(Vector3 start, Vector3 end, float alpha)       => Interpolate(GetEasingFunction(Ease.EaseInBack), start, end, alpha);
        public static Vector3 EaseOutBack(Vector3 start, Vector3 end, float alpha)      => Interpolate(GetEasingFunction(Ease.EaseOutBack), start, end, alpha);
        public static Vector3 EaseInOutBack(Vector3 start, Vector3 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInOutBack), start, end, alpha);
        public static Vector3 EaseInElastic(Vector3 start, Vector3 end, float alpha)    => Interpolate(GetEasingFunction(Ease.EaseInElastic), start, end, alpha);
        public static Vector3 EaseOutElastic(Vector3 start, Vector3 end, float alpha)   => Interpolate(GetEasingFunction(Ease.EaseOutElastic), start, end, alpha);
        public static Vector3 EaseInOutElastic(Vector3 start, Vector3 end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutElastic), start, end, alpha);
    }
}
