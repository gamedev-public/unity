using UnityEngine;

namespace JGDT.Extensions.Easings
{
    /// <summary>
    /// A utility class based on the <see cref="EasingUtility"/> class to get <see cref="Quaternion"/> easing functions. See usage example in class documentation.
    /// </summary>
    /*
     * How to use this class:
     * This class is made to retrieve and cache an easing function for use later. Good use-cases include coroutines or other update-dependent loops.
     * First store an Easing Function found in the EasingUtility class, for example like this:
     * 
     * Function easingFunction = EasingUtility.GetEasingFunction(Ease.EaseInOutQuad);
     * 
     * Then you can make use of this class like so:
     * QuaternionEasings.GetComputedQuaternion(easingFunction, start, end, alpha)
     * 
     * The benefit here is that an easing function is cached and reused until you no longer need it, then you toss it.
     * You could gain more performance if you store all the easing functions you need to use ahead of time and then just
     * use them whenever you need to.
     * 
     * You can also use the provided easing functions in this class, however will be slower due to no function caching.
     */
    public static class QuaternionEasings
    {
        /// <summary>
        /// Used to get an interpolated <see cref="Quaternion"/> from a given type of easing.
        /// </summary>
        /// <param name="easingFunction">The function to use to interpolate.</param>
        /// <param name="start">The <see cref="Quaternion"/> to start interoplating from.</param>
        /// <param name="end">The <see cref="Quaternion"/> to end interpolating to.</param>
        /// <param name="alpha">A value between 0 and 1 representing where in the easing the function is currently evaluating.</param>
        /// <returns>An interpolated <see cref="Quaternion"/> based on the given <see cref="Function"/>.</returns>
        public static Quaternion Interpolate(Function easingFunction, Quaternion start, Quaternion end, float alpha)
        {
            Vector3 startEuler = start.eulerAngles;
            Vector3 endEuler = end.eulerAngles;
            float x = easingFunction(startEuler.x, endEuler.x, alpha);
            float y = easingFunction(startEuler.y, endEuler.y, alpha);
            float z = easingFunction(startEuler.z, endEuler.z, alpha);
            return Quaternion.Euler(x, y, z);
        }

        public static Quaternion Linear(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.Linear), start, end, alpha);
        public static Quaternion Spring(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.Spring), start, end, alpha);
        public static Quaternion EaseInQuad(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInQuad), start, end, alpha);
        public static Quaternion EaseOutQuad(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutQuad), start, end, alpha);
        public static Quaternion EaseInOutQuad(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutQuad), start, end, alpha);
        public static Quaternion EaseInCubic(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInCubic), start, end, alpha);
        public static Quaternion EaseOutCubic(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutCubic), start, end, alpha);
        public static Quaternion EaseInOutCubic(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutCubic), start, end, alpha);
        public static Quaternion EaseInQuart(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInQuart), start, end, alpha);
        public static Quaternion EaseOutQuart(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutQuart), start, end, alpha);
        public static Quaternion EaseInOutQuart(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutQuart), start, end, alpha);
        public static Quaternion EaseInQuint(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInQuint), start, end, alpha);
        public static Quaternion EaseOutQuint(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutQuint), start, end, alpha);
        public static Quaternion EaseInOutQuint(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutQuint), start, end, alpha);
        public static Quaternion EaseInSine(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInSine), start, end, alpha);
        public static Quaternion EaseOutSine(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutSine), start, end, alpha);
        public static Quaternion EaseInOutSine(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutSine), start, end, alpha);
        public static Quaternion EaseInExpo(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInExpo), start, end, alpha);
        public static Quaternion EaseOutExpo(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutExpo), start, end, alpha);
        public static Quaternion EaseInOutExpo(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutExpo), start, end, alpha);
        public static Quaternion EaseInCirc(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInCirc), start, end, alpha);
        public static Quaternion EaseOutCirc(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutCirc), start, end, alpha);
        public static Quaternion EaseInOutCirc(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutCirc), start, end, alpha);
        public static Quaternion EaseInBounce(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInBounce), start, end, alpha);
        public static Quaternion EaseOutBounce(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutBounce), start, end, alpha);
        public static Quaternion EaseInOutBounce(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutBounce), start, end, alpha);
        public static Quaternion EaseInBack(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInBack), start, end, alpha);
        public static Quaternion EaseOutBack(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutBack), start, end, alpha);
        public static Quaternion EaseInOutBack(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutBack), start, end, alpha);
        public static Quaternion EaseInElastic(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInElastic), start, end, alpha);
        public static Quaternion EaseOutElastic(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseOutElastic), start, end, alpha);
        public static Quaternion EaseInOutElastic(Quaternion start, Quaternion end, float alpha) => Interpolate(GetEasingFunction(Ease.EaseInOutElastic), start, end, alpha);
    }
}
