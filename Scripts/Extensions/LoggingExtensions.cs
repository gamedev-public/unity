using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JGDT.Extensions
{
    /// <summary>
    /// A utility class that adds streamlined logging extensions for any class. Example: this.Log(msg).
    /// </summary>
    public static class LoggingExtensions
    {
        /// <summary>
        /// Method to streamline log formatting.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="caller"></param>
        /// <param name="message"></param>
        /// <param name="callerName"></param>
        /// <param name="getHighestClass"></param>
        public static void Log<T>(this T caller, object message, string callerName = "", bool getHighestClass = true)
        {
            LogHidden(caller, message, LoggingEnum.Log, callerName, getHighestClass);
        }

        /// <summary>
        /// Method to streamline log formatting for warnings.
        /// </summary>
        /// <typeparam name="T">The class type</typeparam>
        /// <param name="caller">The caller of the method.</param>
        /// <param name="message">The message object to pass to Unity's Log method.</param>
        /// <param name="callerName">An optional parameter if the debug call should list a name for the caller.</param>
        /// <param name="getHighestClass">Whether to only print the name of the highest class in the hiearchy or the whole class hierarchy.</param>
        public static void LogWarning<T>(this T caller, object message, string callerName = "", bool getHighestClass = true)
        {
            LogHidden(caller, message, LoggingEnum.Warning, callerName, getHighestClass);
        }

        /// <summary>
        /// Method to streamline log formatting for errors.
        /// </summary>
        /// <typeparam name="T">The class type</typeparam>
        /// <param name="caller">The caller of the method.</param>
        /// <param name="message">The message object to pass to Unity's Log method.</param>
        /// <param name="callerName">An optional parameter if the debug call should list a name for the caller.</param>
        /// <param name="getHighestClass">Whether to only print the name of the highest class in the hiearchy or the whole class hierarchy.</param>
        public static void LogError<T>(this T caller, object message, string callerName = "", bool getHighestClass = true)
        {
            LogHidden(caller, message, LoggingEnum.Error, callerName, getHighestClass);
        }

        /// <summary>
        /// The logging method that does the actual logging call, given an enum value.
        /// </summary>
        /// <typeparam name="T">The class type</typeparam>
        /// <param name="caller">The caller of the method.</param>
        /// <param name="message">The message object to pass to Unity's Log method.</param>
        /// <param name="logType">What type of logging it is.</param>
        /// <param name="callerName">An optional parameter if the debug call should list a name for the caller.</param>
        /// <param name="getHighestClass">Whether to only print the name of the highest class in the hiearchy or the whole class hierarchy.</param>
        private static void LogHidden<T>(this T caller, object message, LoggingLevel logType, string callerName = "", bool getHighestClass = true)
        {
            string callerString = $"[{(getHighestClass == true ? caller.GetHighestClassName() : $"{typeof(T)}")}]";
            if (!string.IsNullOrEmpty(callerName)) callerString += $"[{callerName}]";
            switch (logType)
            {
                case LoggingLevel.Log:
                    if (caller is Object) Debug.Log($"{callerString}:" + message, caller as Object);
                    else Debug.Log($"{callerString}:" + message);
                    break;
                case LoggingLevel.Warning:
                    if (caller is Object) Debug.LogWarning($"{callerString}:" + message, caller as Object);
                    else Debug.LogWarning($"{callerString}:" + message);
                    break;
                case LoggingLevel.Error:
                    if (caller is Object) Debug.LogError($"{callerString}:" + message, caller as Object);
                    else Debug.LogError($"{callerString}:" + message);
                    break;
            }
        }

        /// <summary>
        /// Extension method used to return the highest class name in the class hierarchy.
        /// </summary>
        /// <typeparam name="T">The class type.</typeparam>
        /// <param name="obj">The object to call the method on.</param>
        /// <returns>A string containing the name of the highest class in the hierarchy.</returns>
        private static string GetHighestClassName<T>(this T obj)
        {
            string classString = $"{typeof(T)}";
            if (classString.Contains("."))
            {
                string[] splitString = classString.Split('.');
                return splitString[splitString.Length - 1];
            }
            else
            {
                return classString;
            }
        }
    }

    public enum LoggingEnum
    {
        Log,
        Warning,
        Error
    }
}
