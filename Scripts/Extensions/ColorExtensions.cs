﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JGDT.Extensions
{
    /// <summary>
    /// An extension and utility class to deal with <see cref="Color"/>s.
    /// </summary>
    public static class ColorExtensions
    {
        /// <summary>
        /// Will convert a hex string using unity's <see cref="ColorUtility"/> + an alpha value.
        /// </summary>
        /// <param name="hexValue">The string to parse.</param>
        /// <param name="alpha">The desired alpha value of the color.</param>
        /// <returns>The converted color.</returns>
        public static Color FromHexToColor(string hexValue, float alpha = 1f)
        {
            TryParseHexString(hexValue, alpha, out Color color);
            return color;
        }

        /// <summary>
        /// Will convert a hexadecimal value using unity's <see cref="ColorUtility"/> + an alpha value.
        /// </summary>
        /// <param name="hexValue">The decimal to parse in the format of 0x000 or 0x000000.</param>
        /// <param name="alpha">The desired alpha value of the color.</param>
        /// <param name="color">The resulting color.</param>
        /// <returns>The converted color.</returns>
        public static Color FromHexToColor(int hexValue, float alpha = 1f)
        {
            TryParseHexString(hexValue, alpha, out Color color);
            return color;
        }

        /// <summary>
        /// Will try and parse a hex string using unity's <see cref="ColorUtility"/> + an alpha value.
        /// </summary>
        /// <param name="hexValue">The string to parse.</param>
        /// <param name="alpha">The desired alpha value of the color.</param>
        /// <param name="color">The resulting color.</param>
        /// <returns>True if the parsing succeeded otherwise false.</returns>
        public static bool TryParseHexString(string hexValue, float alpha, out Color color)
        {
            bool result = ColorUtility.TryParseHtmlString(hexValue.ToUpper(), out color);
            color = new Color(color.r, color.g, color.b, alpha);
            return result;
        }

        /// <summary>
        /// Will try and parse a hexadecimal value using unity's <see cref="ColorUtility"/> + an alpha value.
        /// </summary>
        /// <param name="hexValue">The decimal to parse in the format of 0x000 or 0x000000.</param>
        /// <param name="alpha">The desired alpha value of the color.</param>
        /// <param name="color">The resulting color.</param>
        /// <returns>True if the parsing succeeded otherwise false.</returns>
        public static bool TryParseHexString(int hexValue, float alpha, out Color color)
        {
            bool result = ColorUtility.TryParseHtmlString($"{hexValue}".ToUpper(), out color);
            color = new Color(color.r, color.g, color.b, alpha);
            return result;
        }
    }
}
