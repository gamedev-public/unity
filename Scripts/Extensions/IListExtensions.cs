﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JGDT.Extensions
{
    /// <summary>
    /// A utility class that adds useful extensions for any <see cref="IList{T}"/>.
    /// </summary>
    public static class IListExtensions
    {
        /// <summary>
        /// Swap two objects around in an <see cref="IList{T}"/>.
        /// </summary>
        /// <typeparam name="T">The type of element in the <see cref="IList{T}"/>.</typeparam>
        /// <param name="ilist">The <see cref="IList{T}"/> that needs to have elements swapped.</param>
        /// <param name="firstIndex">The index of the first item in the <see cref="IList{T}"/> to swap.</param>
        /// <param name="secondIndex">The index of the second item in the <see cref="IList{T}"/> to swap.</param>
        public static void Swap<T>(this IList<T> ilist, int firstIndex, int secondIndex)
        {
            if (firstIndex >= ilist.Count || secondIndex >= ilist.Count) 
                throw new ArgumentOutOfRangeException("firstIndex and/or secondIndex is out of Range.");
            T t = ilist[firstIndex];
            T t2 = ilist[secondIndex];
            ilist[firstIndex] = t2;
            ilist[secondIndex] = t;
        }

        /// <summary>
        /// Shuffle an <see cref="IList{T}"/> using Fisher-Yates Shuffle.
        /// </summary>
        /// <typeparam name="T">The type of element in the <see cref="IList{T}"/>.</typeparam>
        /// <param name="ilist">The <see cref="IList{T}"/> to shuffle.</param>
        public static void Shuffle<T>(this IList<T> ilist)
        {
            for (int index = 0; index < ilist.Count; index++)
            {
                int rnd = UnityEngine.Random.Range(index, ilist.Count);
                ilist.Swap(rnd, index);
            }
        }

        /// <summary>
        /// Shuffle an <see cref="IList{T}"/> using Fisher-Yates Shuffle with a seed.
        /// </summary>
        /// <typeparam name="T">The type of element in the <see cref="IList{T}"/>.</typeparam>
        /// <param name="ilist">The <see cref="IList{T}"/> to shuffle.</param>
        /// <param name="seed">The seed for this Shuffle.</param>
        public static void Shuffle<T>(this IList<T> ilist, int seed)
        {
            UnityEngine.Random.InitState(seed);
            for (int index = 0; index < ilist.Count; index++)
            {
                int rnd = UnityEngine.Random.Range(index, ilist.Count);
                ilist.Swap(rnd, index);
            }
        }
    }
}
