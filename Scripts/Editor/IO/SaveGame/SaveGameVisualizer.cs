using JGDT.IO.SaveGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace JGDTEditor.IO.SaveGame
{
    public class SaveGameVisualizer : EditorWindow
    {
        private Rect _topPanelRect;
        private Rect _leftPanelRect;
        private Rect _infoPanelRect;
        private Texture2D _lineTex;
        private Color _darkGrey = new Color(65f / 255, 65f / 255, 65f / 255);

        private SaveGameVisualizerSettings _settings;
        private float _topPanelVerticalSpace = 18f;
        private float _leftPanelHorizontalSpace = 208f;

        private Rect _saveListRect = default;
        private Dictionary<Rect, string> _saveListKeys = new Dictionary<Rect, string>();
        private string _markedSave = string.Empty;
        private GameState _markedState = null;

        [MenuItem("Window/Save Game Visualizer")]
        public static void ShowWindow()
        {
            GetWindow(typeof(SaveGameVisualizer), false, "JGDT Save Game Visualizer");
        }

        #region Top Panel
        private void DrawTopPanel()
        {
            GUI.color = Color.gray;
            GUI.DrawTexture(_topPanelRect, _lineTex);
            GUI.color = Color.white;
            GUI.Label(_settings.PathLabelRect, _settings.PathLabelText, EditorStyles.boldLabel);
            GUI.Label(_settings.PathRect, _settings.PathText);
        }
        #endregion

        #region Left Side Panel
        private void DrawLeftPanel()
        {
            GUI.color = Color.gray;
            GUI.DrawTexture(_leftPanelRect, _lineTex);

            GUI.color = Color.white;
            bool IsScanPressed = GUI.Button(_settings.ScanButtonRect, _settings.ScanButtonText, EditorStyles.miniButton);
            if (IsScanPressed) Scan();

            GUI.Label(_settings.ScanListLabelRect, _settings.ScanListText, EditorStyles.boldLabel);
            _saveListRect = new Rect(
                    new Vector2(_settings.ScanListLabelRect.xMin, _settings.ScanListLabelRect.yMax + _settings.LeftPanelVerticalPadding),
                    new Vector2(
                        _leftPanelRect.width - (_settings.LeftPanelHorizontalPadding * 2),
                        (_settings.TextFieldHeight * (_saveListKeys.Count + 1)))
                    );
            GUI.color = _darkGrey;
            GUI.DrawTexture(_saveListRect, _lineTex);
            if (_saveListKeys.Count > 0)
            {
                foreach (KeyValuePair<Rect, string> pair in _saveListKeys)
                {
                    if (pair.Value == _markedSave)
                    {
                        GUI.color = Color.blue;
                        GUI.DrawTexture(pair.Key, _lineTex);
                    }
                    GUI.color = Color.white;
                    GUI.Label(pair.Key, pair.Value);
                }
            }
        }

        private void Scan()
        {
            List<SaveFileInfo> infos = GamePersistence.GetAllGameStateFileInfos();
            _saveListKeys.Clear();
            _markedSave = string.Empty;
            if (infos.Count == 0)
            {
                _saveListRect = default;
            }
            else
            {
                Rect rect;
                Rect lastRect = default;
                for (int index = 0; index < infos.Count; index++)
                {
                    if (index == 0)
                    {
                        rect = new Rect(
                            new Vector2(_saveListRect.xMin + _settings.LeftPanelHorizontalPadding, _saveListRect.yMin + _settings.LeftPanelVerticalPadding),
                            new Vector2(_saveListRect.width - (_settings.LeftPanelHorizontalPadding * 2), _settings.TextFieldHeight));
                        lastRect = rect;
                    }
                    else
                    {
                        rect = new Rect(
                            new Vector2(_saveListRect.xMin + _settings.LeftPanelHorizontalPadding, lastRect.yMax + _settings.LeftPanelVerticalPadding),
                            new Vector2(lastRect.width, _settings.TextFieldHeight));
                        lastRect = rect;
                    }
                    _saveListKeys.Add(rect, infos[index].Key);
                }
            }
        }
        #endregion

        #region Info Panel
        private void DrawInfoPanel()
        {
            GUI.color = Color.white;
            _infoPanelRect = new Rect(new Vector2(_leftPanelRect.xMax, _topPanelRect.yMax), new Vector2(position.width, position.height));
            GUI.DrawTexture(_infoPanelRect, _lineTex);
            if (_markedSave != string.Empty)
            {
                int longestName = 0;
                int longestValue = 0;
                Dictionary<string, string> parsed = new Dictionary<string, string>();
                foreach (KeyValuePair<string, string> pair in _markedState.Entries)
                {
                    if (pair.Key.Length > longestName) longestName = pair.Key.Length + 2;
                    object entry = _markedState.GetKey<object>(pair.Key);
                    if (entry.ToString().Length > longestValue) longestValue = entry.ToString().Length + 2;
                    parsed.Add(pair.Key, entry.ToString());
                }

                Rect InfoPanelKeyColumnLabelRect = new Rect(
                    new Vector2(_infoPanelRect.xMin, _infoPanelRect.yMin),
                    new Vector2(_settings.CharacterWidth * longestName, _settings.TextFieldHeight));
                Rect InfoPanelValueColumnLabelRect = new Rect(
                    new Vector2(InfoPanelKeyColumnLabelRect.xMax, InfoPanelKeyColumnLabelRect.yMin),
                    new Vector2(_settings.CharacterWidth * longestValue, _settings.TextFieldHeight));
                GUI.color = Color.white;
                GUI.Label(InfoPanelKeyColumnLabelRect, "Key", EditorStyles.miniButton);
                GUI.Label(InfoPanelValueColumnLabelRect, "Value", EditorStyles.miniButton);
                Rect lastKey = InfoPanelKeyColumnLabelRect;
                foreach (KeyValuePair<string, string> entries in parsed)
                {
                    Rect keyRect = new Rect(
                        new Vector2(InfoPanelKeyColumnLabelRect.xMin, lastKey.yMax + _settings.LeftPanelVerticalPadding),
                        new Vector2(InfoPanelKeyColumnLabelRect.width, _settings.TextFieldHeight));
                    Rect valueRect = new Rect(
                        new Vector2(InfoPanelKeyColumnLabelRect.xMax, lastKey.yMax + _settings.LeftPanelVerticalPadding),
                        new Vector2(InfoPanelValueColumnLabelRect.width, _settings.TextFieldHeight));
                    GUI.Label(keyRect, entries.Key, EditorStyles.miniButton);
                    GUI.Label(valueRect, entries.Value.Replace("\r\n", string.Empty), EditorStyles.miniButton);
                    lastKey = keyRect;
                }
            }
        }
        #endregion

        #region Left-Click
        private void HandleLeftClick()
        {
            Event current = Event.current;

            // Left-Click
            foreach (KeyValuePair<Rect, string> pair in _saveListKeys)
            {
                if (pair.Key.Contains(current.mousePosition))
                {
                    if (current.type == EventType.MouseDown)
                    {
                        try
                        {
                            GameState state = GamePersistence.Load(pair.Value);
                            _markedSave = pair.Value;
                            _markedState = state;
                            Repaint();
                        }
                        catch (ArgumentException ex)
                        {
                            Debug.LogWarning(ex.Message + ". Forcing a new Scan() call.");
                            Scan();
                        }
                        break;
                    }
                }
            }
        }

        private void ShowGameState()
        {

        }
        #endregion

        private void OnGUI()
        {
            _topPanelRect = new Rect(Vector2.zero, new Vector2(position.size.x, _topPanelVerticalSpace));
            _leftPanelRect = new Rect(new Vector2(0f, _topPanelVerticalSpace), new Vector2(_leftPanelHorizontalSpace, position.size.y));
            _lineTex = new Texture2D(1, 1);
            if (_settings == null) _settings = new SaveGameVisualizerSettings(_topPanelRect, _leftPanelRect, GamePersistence.SaveLocation);
            _settings.TopPanelRect = _topPanelRect;
            _settings.LeftPanelRect = _leftPanelRect;
            DrawTopPanel();
            DrawLeftPanel();
            DrawInfoPanel();
            HandleLeftClick();
        }
    }

}