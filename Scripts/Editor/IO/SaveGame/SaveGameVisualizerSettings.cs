﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JGDTEditor.IO.SaveGame
{
    public class SaveGameVisualizerSettings
    {
        public Rect TopPanelRect;
        public Rect LeftPanelRect;

        // Top Panel
        public Rect PathLabelRect;
        public Rect PathRect;
        public string PathLabelText = "Path:";
        public string PathText = "";
        // Left Panel
        public float LeftPanelHorizontalPadding = 4f;
        public float LeftPanelVerticalPadding = 4f;
        public float CharacterWidth = 7f;
        public float TextFieldHeight = 16f;
        public Rect ScanButtonRect;
        public string ScanButtonText = "Scan Saves";
        public Rect ScanListLabelRect;
        public string ScanListText = "Scanned Saves:";

        public SaveGameVisualizerSettings(Rect topPanelRect, Rect leftPanelRect, string path)
        {
            TopPanelRect = topPanelRect;
            LeftPanelRect = leftPanelRect;
            PathText = path;
            MakeRects();
        }

        private void MakeRects()
        {
            PathLabelRect = new Rect(
                new Vector2(TopPanelRect.xMin, TopPanelRect.yMin),
                new Vector2(CharacterWidth * PathLabelText.Length, TextFieldHeight));
            PathRect = new Rect(
                new Vector2(PathLabelRect.xMax, TopPanelRect.yMin),
                new Vector2(CharacterWidth * (PathText.Length + 2), TextFieldHeight));

            ScanButtonRect = new Rect(
                new Vector2(TopPanelRect.xMin + LeftPanelHorizontalPadding, TopPanelRect.yMax + LeftPanelVerticalPadding),
                new Vector2(LeftPanelRect.width - 8, TextFieldHeight));
            ScanListLabelRect = new Rect(
                new Vector2(ScanButtonRect.xMin, ScanButtonRect.yMax + LeftPanelVerticalPadding),
                new Vector2(CharacterWidth * ScanListText.Length, TextFieldHeight));
        }
    }
}
