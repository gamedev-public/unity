﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace JGDTEditor.IMGUI.DialogueEditor
{
    [Serializable]
    public class DialogueChain : ScriptableObject
    {
        public Dictionary<Guid, DialogueNode> Nodes = new Dictionary<Guid, DialogueNode>();
    }
}
