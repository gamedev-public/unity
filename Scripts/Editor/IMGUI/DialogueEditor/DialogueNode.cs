﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JGDTEditor.IMGUI.DialogueEditor
{
    public struct DialogueNode
    {
        /// <summary>
        /// Used by the <see cref="DialogueEditor"/> to know where the <see cref="DialogueNode"/> is on the canvas.
        /// </summary>
        public Rect CanvasRect;
        /// <summary>
        /// The <see cref="Guid"/> to refer to this <see cref="DialogueNode"/>.
        /// </summary>
        public Guid Id;
        /// <summary>
        /// The name that shows up on the Node in the editor.
        /// </summary>
        public string Label;
        /// <summary>
        /// The title of this dialogue (if needed).
        /// </summary>
        public string Title;
        /// <summary>
        /// Name of the one speaking (if needed).
        /// </summary>
        public string Name;
        /// <summary>
        /// The text to show for this dialogue.
        /// </summary>
        public string Dialogue;
        /// <summary>
        /// The image for this dialogue (if any).
        /// </summary>
        public Image Portrait;
        /// <summary>
        /// The dialogues after this one.
        /// </summary>
        public List<Guid> Next;
    }
}
