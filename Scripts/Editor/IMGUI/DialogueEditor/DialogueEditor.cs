using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace JGDTEditor.IMGUI.DialogueEditor
{
    public class DialogueEditor : EditorWindow
    {
        // Editor Properties
        private DialogueEditorSettings _settings;
        private float _leftPanelHorizontalSpace = 208f;
        private float _topPanelVerticalSpace = 18f;

        // Editor Data
        private static int _instanceId = int.MaxValue;
        private Texture2D _lineTex;
        private static Dictionary<Guid, DialogueNode> _nodes = new Dictionary<Guid, DialogueNode>();
        private static Guid _startNode = Guid.Empty;
        private Guid _mousedNode = Guid.Empty;
        private bool _mouseDown = false;
        private bool _contextVisible = false;
        private bool _isConnecting = false;
        private Rect _topPanelRect;
        private Rect _leftPanelRect;
        private Rect _gridRect;
        private int _nodeIndex = 0;

        [MenuItem("Window/Dialogue Editor")]
        public static void ShowWindow()
        {
            UnityEngine.Object select = Selection.activeObject;
            if (select != null && select is DialogueChain)
            {
                _instanceId = Selection.activeObject.GetInstanceID();
                _nodes = (select as DialogueChain).Nodes;
                _startNode = _nodes.First(pair => pair.Value.Label == "Start").Key;
            }
            GetWindow(typeof(DialogueEditor), false, "JGDT Dialogue Editor");
        }

        #region Connection
        private void DrawLineToMouse()
        {
            if (_isConnecting == true)
            {
                Event current = Event.current;
                if (current.keyCode == KeyCode.Escape)
                {
                    _isConnecting = false;
                }
                else
                {
                    GUILayout.BeginHorizontal();
                    DialogueNode node = _nodes[_mousedNode];
                    Vector2 nodePos = node.CanvasRect.position;
                    Vector2 mousePos = current.mousePosition;

                    if (current.type == EventType.Repaint)
                    {
                        GUI.BeginClip(_gridRect);
                        GL.PushMatrix();
                        GL.Begin(GL.LINES);
                        GL.Color(Color.cyan);
                        GL.Vertex3(nodePos.x - (node.CanvasRect.width + _settings.GridPadding), nodePos.y, 0);
                        GL.Vertex3(mousePos.x - (node.CanvasRect.width * 1.6f), mousePos.y, 0);
                        GL.End();

                        GL.PopMatrix();
                        GUI.EndClip();
                    }
                    GUILayout.EndHorizontal();
                }
            }
        }

        private void DrawLineBetweenNodes()
        {
            Rect window = new Rect(Vector2.zero, position.size);
            GUILayout.BeginArea(window);
            try
            {
                if (Event.current.type == EventType.Repaint)
                {
                    GUI.BeginClip(_gridRect);
                    GL.PushMatrix();
                    GL.Begin(GL.LINES);
                    foreach (DialogueNode startNode in _nodes.Values)
                    {
                        foreach (Guid endGuid in startNode.Next)
                        {
                            GL.Color(Color.green);
                            DialogueNode endNode = _nodes[endGuid];
                            Vector2 startPos = _nodes[startNode.Id].CanvasRect.position;
                            Vector2 endPos = endNode.CanvasRect.position;
                            GL.Vertex3(startPos.x - (endNode.CanvasRect.width * 1.15f), startPos.y, 0);
                            GL.Vertex3(endPos.x - (endNode.CanvasRect.width * 1.15f), endPos.y, 0);
                        }
                    }
                    GL.End();
                    GL.PopMatrix();
                    GUI.EndClip();
                }
                GUILayout.EndArea();
            }
            catch (Exception) { }
        }
        #endregion

        #region Top Panel
        private void DrawTopPanel()
        {
            bool isLoadPressed = GUI.Button(_settings.LoadButtonRect, _settings.LoadButtonText, EditorStyles.miniButton);
            bool isSavePressed = GUI.Button(_settings.SaveButtonRect, _settings.SaveButtonText, EditorStyles.miniButton);
            bool isSaveAsPressed = GUI.Button(_settings.SaveAsButtonRect, _settings.SaveAsButtonText, EditorStyles.miniButton);

            if (isLoadPressed) Load();

            if (isSavePressed)
            {
                // Existing DialogueChain
                if (_instanceId != int.MaxValue)
                {
                    Save();
                }
                // New DialogueChain
                else
                {
                    SaveAs();
                }
            }

            if (isSaveAsPressed) SaveAs();
        }

        private void Load()
        {
            string path = EditorUtility.OpenFilePanel("Open DialogueChain", "Assets", "asset");
            if (!string.IsNullOrEmpty(path))
            {
                path = $"{path.Substring(path.IndexOf("Assets"))}";
                DialogueChain chain = AssetDatabase.LoadAssetAtPath<DialogueChain>(path);
                if (chain != null)
                {
                    _nodes = chain.Nodes;
                    _instanceId = chain.GetInstanceID();
                }
            }
        }

        private void Save()
        {
            string path = AssetDatabase.GetAssetPath(_instanceId);
            if (!string.IsNullOrEmpty(path))
            {
                AssetDatabase.LoadAssetAtPath<DialogueChain>(path).Nodes = _nodes;
            }
            else
            {
                SaveAs();
            }
        }

        private void SaveAs()
        {
            string path = EditorUtility.SaveFilePanel("Save DialogueChain", "Assets", "NewDialogueChain", "asset");
            if (!string.IsNullOrEmpty(path))
            {
                DialogueChain chain = CreateInstance<DialogueChain>();
                chain.Nodes = _nodes;
                chain.name = $"{path.Split('/').Last().Replace(".asset", "")}";
                path = $"{path.Substring(path.IndexOf("Assets"))}";
                AssetDatabase.CreateAsset(chain, path);
                _instanceId = AssetDatabase.LoadAssetAtPath<DialogueChain>(path).GetInstanceID();
            }
        }
        #endregion

        #region Left Side Panel
        private void DrawLeftPanel()
        {
            GUI.color = Color.grey;
            GUI.DrawTexture(_leftPanelRect, _lineTex);

            GUI.color = Color.white;

            if (_mousedNode == Guid.Empty)
            {
                GUI.Label(_settings.NoSelectionRect, "...");
            }
            else
            {
                DialogueNode node = _nodes[_mousedNode];
                GUI.Label(_settings.NodeLabelLabelRect, "Label", EditorStyles.boldLabel);
                if (node.Label == "Start")
                {
                    GUI.Label(_settings.NodeLabelFieldRect, node.Label);
                }
                else
                {
                    node.Label = GUI.TextField(_settings.NodeLabelFieldRect, node.Label);
                }
                GUI.Label(_settings.NodeTitleLabelRect, "Title", EditorStyles.boldLabel);
                node.Title = GUI.TextField(_settings.NodeTitleFieldRect, node.Title);
                GUI.Label(_settings.NodeNameLabelRect, "Name", EditorStyles.boldLabel);
                node.Name = GUI.TextField(_settings.NodeNameFieldRect, node.Name);
                GUI.Label(_settings.NodeDialogueLabelRect, "Dialogue", EditorStyles.boldLabel);
                node.Dialogue = GUI.TextArea(_settings.NodeDialogueFieldRect, node.Dialogue);
                GUI.Label(_settings.NodePortraitLabelRect, "Portrait", EditorStyles.boldLabel);
                node.Portrait = (Image)EditorGUI.ObjectField(_settings.NodePortraitFieldRect, node.Portrait, typeof(Image), false);
                GUI.Label(_settings.NodeNextLabelRect, "Next Dialogue(s)", EditorStyles.boldLabel);
                List<(string, Rect)> layouts = _settings.GetNextDialogueLayout(node.Next, _nodes);
                if (layouts.Count > 0)
                {
                    foreach ((string, Rect) layout in layouts)
                    {
                        GUI.Label(layout.Item2, layout.Item1);
                    }
                }
                else
                {
                    GUI.Label(_settings.NodeNextFieldRect, "None");
                }

                _nodes[_mousedNode] = node;
            }
        }
        #endregion

        #region Grid
        private void DrawHorizontal(float pos, float padding, Color color)
        {
            Vector2 start = new Vector2(_leftPanelHorizontalSpace, pos + padding + _topPanelVerticalSpace);
            Vector2 scale = new Vector2(position.size.x, 1f);
            GUI.color = color;
            GUI.DrawTexture(new Rect(start, scale), _lineTex);
        }

        private void DrawVertical(float pos, float padding, Color color)
        {
            Vector2 start = new Vector2((pos + _leftPanelHorizontalSpace) + padding, _topPanelVerticalSpace);
            Vector2 scale = new Vector2(1f, position.size.y);
            GUI.color = color;
            GUI.DrawTexture(new Rect(start, scale), _lineTex);
        }

        private void DrawGrid(float padding, Color color)
        {
            float rgbValue = 56f / 255f;
            GUI.color = new Color(rgbValue, rgbValue, rgbValue);
            GUI.DrawTexture(_gridRect, _lineTex);
            for (int pos = 0; pos < position.size.y; pos++)
            {
                DrawVertical(pos, pos * padding, color);
            }
            for (int pos = 0; pos < position.size.x; pos++)
            {
                DrawHorizontal(pos, pos * padding, color);
            }
        }

        private void DrawNodes()
        {
            foreach (DialogueNode node in _nodes.Values)
            {
                GUI.color = Color.white;
                string label = node.Label;
                if (node.Label.Length > _settings.NodeCharacterLimit)
                {
                    label = $"{node.Label.Substring(0, 13)}...";
                }
                GUI.Box(node.CanvasRect, label, EditorStyles.toolbarButton);
            }
        }
        #endregion

        #region Left-Click
        private void HandleLeftClick()
        {
            Event current = Event.current;

            // Left-Click
            if (_gridRect.Contains(current.mousePosition))
            {
                switch (current.type)
                {
                    case EventType.MouseDown:
                        if (_mousedNode == Guid.Empty)
                        {
                            if (IsMouseOverNode(current.mousePosition, out Guid nodeId))
                            {
                                _mousedNode = nodeId;
                                _mouseDown = true;
                            }
                            else
                            {
                                _contextVisible = false;
                            }
                        }
                        else
                        {
                            if (IsMouseOverNode(current.mousePosition, out Guid nodeId))
                            {
                                if (_mousedNode != nodeId)
                                {
                                    if (_isConnecting == true)
                                    {
                                        DialogueNode node = _nodes[_mousedNode];
                                        if (!node.Next.Contains(nodeId)) node.Next.Add(nodeId);
                                        _isConnecting = false;
                                    }
                                    _mousedNode = nodeId;
                                }
                                if (_mousedNode != nodeId) _mousedNode = nodeId;
                                _mouseDown = true;
                            }
                            else
                            {
                                if (_contextVisible == true)
                                {
                                    _contextVisible = false;
                                }
                                else
                                {
                                    _mousedNode = Guid.Empty;
                                }
                                _isConnecting = false;
                            }
                        }
                        break;
                    case EventType.MouseUp:
                        _mouseDown = false;
                        break;
                    case EventType.MouseDrag:
                        if (_mousedNode != Guid.Empty && _mouseDown == true)
                        {
                            DialogueNode node = _nodes[_mousedNode];
                            node.CanvasRect.position = new Vector2(
                                current.mousePosition.x - (node.CanvasRect.width * 0.5f),
                                current.mousePosition.y - (node.CanvasRect.height * 0.5f));
                            current.Use();
                            _nodes[_mousedNode] = node;
                        }
                        break;
                }
                Repaint();
            }
        }
        #endregion

        #region Right-Click
        private void HandleRightClick()
        {
            Event current = Event.current;

            // Right-Click
            if (_gridRect.Contains(current.mousePosition) && current.type == EventType.ContextClick)
            {
                _contextVisible = true;
                GenericMenu menu = new GenericMenu();
                if (IsMouseOverNode(current.mousePosition, out Guid nodeId))
                {
                    menu.AddItem(new GUIContent("Connect Node"), false, () =>
                    {
                        _isConnecting = true;
                        _mousedNode = nodeId;
                    });
                    menu.AddItem(new GUIContent("Delete Node"), false, () =>
                    {
                        if (_startNode != nodeId)
                        {
                            _nodes.Remove(nodeId);
                            _nodes.Where(pair => pair.Value.Next.Contains(nodeId)).ToList().ForEach(pair => pair.Value.Next.Remove(nodeId));
                            if (_mousedNode == nodeId) _mousedNode = Guid.Empty;
                        }
                        else
                        {
                            EditorUtility.DisplayDialog("Error", "Cannot delete the Start node.", "Close");
                        }
                    });

                    if (_nodes[nodeId].Next.Count > 0)
                    {
                        foreach (Guid guid in _nodes[nodeId].Next)
                        {
                            menu.AddItem(new GUIContent($"Delete Connection to {_nodes[guid].Label}"), false, () =>
                            {
                                _nodes[nodeId].Next.Remove(guid);
                            });
                        }
                    }
                }
                else
                {
                    menu.AddItem(new GUIContent("Add Node"), false, () => AddNode(current.mousePosition));
                    menu.AddItem(new GUIContent("Delete All Nodes"), false, () =>
                    {
                        _contextVisible = false;
                        _mousedNode = Guid.Empty;
                        _isConnecting = false;
                        _nodes.Clear();
                        Repaint();
                    });
                }
                menu.ShowAsContext();

                current.Use();
            }
        }

        /// <summary>
        /// Convenience method to check if a right-click was done on a <see cref="DialogueNode"/>
        /// </summary>
        /// <param name="mousePosition">The position of the mouse.</param>
        /// <param name="nodeRect">The nodes <see cref="Rect"/></param>
        /// <returns>True if the right-click was on a node, otherwise false.</returns>
        private bool IsMouseOverNode(Vector2 mousePosition, out Guid nodeId)
        {
            nodeId = _nodes.Values.FirstOrDefault(value => value.CanvasRect.Contains(mousePosition)).Id;
            return nodeId != Guid.Empty;
        }

        private Guid AddNode(Vector2 mousePos) => AddNode($"Node-{_nodeIndex}", mousePos);

        private Guid AddNode(string nodeLabel, Vector2 mousePos)
        {
            float characterWidth = 8f;
            float nodeHeight = 24f;
            Guid id = Guid.NewGuid();
            Rect rect = new Rect(mousePos, new Vector2(_settings.NodeCharacterLimit * characterWidth, nodeHeight));
            DialogueNode node = new DialogueNode
            {
                Id = id,
                Label = nodeLabel,
                CanvasRect = rect,
                Next = new List<Guid>()
            };
            _nodes.Add(id, node);
            _nodeIndex += 1;
            return id;
        }

        #endregion

        private void OnGUI()
        {
            _topPanelRect = new Rect(Vector2.zero, new Vector2(position.size.x, _topPanelVerticalSpace));
            _leftPanelRect = new Rect(new Vector2(0f, _topPanelVerticalSpace), new Vector2(_leftPanelHorizontalSpace, position.size.y));
            _gridRect = new Rect(new Vector2(_leftPanelHorizontalSpace, _topPanelVerticalSpace), position.size);
            _lineTex = new Texture2D(1, 1);
            if (_settings == null) _settings = new DialogueEditorSettings(_topPanelRect, _leftPanelRect);
            _settings.TopPanelRect = _topPanelRect;
            _settings.LeftPanelRect = _leftPanelRect;
            if (_nodes.Count == 0) _startNode = AddNode("Start", _gridRect.center);
            DrawTopPanel();
            DrawLeftPanel();
            DrawGrid(_settings.GridPadding, Color.grey);
            HandleRightClick();
            HandleLeftClick();
            DrawLineBetweenNodes();
            DrawLineToMouse();
            DrawNodes();
        }

        private void OnDestroy()
        {
            _instanceId = int.MaxValue;
            _nodes = new Dictionary<Guid, DialogueNode>();
            _mousedNode = Guid.Empty;
            _mouseDown = false;
            _contextVisible = false;
            _isConnecting = false;
            _nodeIndex = 0;
        }
    }
}