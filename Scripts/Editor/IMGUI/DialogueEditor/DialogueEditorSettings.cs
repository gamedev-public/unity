﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace JGDTEditor.IMGUI.DialogueEditor
{
    public class DialogueEditorSettings
    {
        public Rect TopPanelRect;
        public Rect LeftPanelRect;

        // Top Panel
        public string LoadButtonText = "Load";
        public string SaveButtonText = "Save";
        public string SaveAsButtonText = "Save As..";
        public Rect LoadButtonRect;
        public Rect SaveButtonRect;
        public Rect SaveAsButtonRect;
        // Grid
        public float GridPadding = 16f;
        // Character Fields
        public int NodeCharacterLimit = 16;
        // Left Panel
        public Rect NoSelectionRect;
        public float LeftPanelHorizontalPadding = 4f;
        public float LeftPanelVerticalPadding = 4f;
        public float TextFieldCharacterLimit = 16f;
        public float CharacterWidth = 8f;
        public float TextFieldHeight = 16f;
        public float TextAreaHeight = 16f * 6;
        // Left Panel Labels
        public Rect NodeLabelLabelRect;
        public Rect NodeTitleLabelRect;
        public Rect NodeNameLabelRect;
        public Rect NodeDialogueLabelRect;
        public Rect NodePortraitLabelRect;
        public Rect NodeNextLabelRect;
        // Left Panel Fields
        public Rect NodeLabelFieldRect;
        public Rect NodeTitleFieldRect;
        public Rect NodeNameFieldRect;
        public Rect NodeDialogueFieldRect;
        public Rect NodePortraitFieldRect;
        public Rect NodeNextFieldRect;

        public DialogueEditorSettings(Rect topPanelRect, Rect leftPanelRect)
        {
            TopPanelRect = topPanelRect;
            LeftPanelRect = leftPanelRect;

            NoSelectionRect = new Rect(
                new Vector2(LeftPanelRect.min.x + LeftPanelHorizontalPadding, LeftPanelRect.min.y + LeftPanelVerticalPadding),
                new Vector2(TextFieldCharacterLimit * CharacterWidth, TextFieldHeight));
            MakeRects();
        }

        private void MakeRects()
        {
            LoadButtonRect = new Rect(
                new Vector2(TopPanelRect.xMin, TopPanelRect.yMin),
                new Vector2(CharacterWidth * (LoadButtonText.Length + 2), TextFieldHeight));
            SaveButtonRect = new Rect(
                new Vector2(LoadButtonRect.xMax, TopPanelRect.yMin),
                new Vector2(CharacterWidth * (SaveButtonText.Length + 2), TextFieldHeight));
            SaveAsButtonRect = new Rect(
                 new Vector2(SaveButtonRect.xMax, TopPanelRect.yMin),
                 new Vector2(CharacterWidth * (SaveAsButtonText.Length + 2), TextFieldHeight));

            NodeLabelLabelRect = NoSelectionRect;
            NodeLabelFieldRect = new Rect(
                new Vector2(NodeLabelLabelRect.xMax * 0.5f + LeftPanelHorizontalPadding, NodeLabelLabelRect.min.y),
                new Vector2(TextFieldCharacterLimit * CharacterWidth, TextFieldHeight));

            NodeTitleLabelRect = new Rect(
                new Vector2(NodeLabelLabelRect.xMin, NodeLabelLabelRect.max.y + LeftPanelVerticalPadding * 0.5f),
                NodeLabelLabelRect.size);
            NodeTitleFieldRect = new Rect(
                new Vector2(NodeTitleLabelRect.xMax * 0.5f + LeftPanelHorizontalPadding, NodeTitleLabelRect.min.y + LeftPanelVerticalPadding * 0.5f),
                new Vector2(TextFieldCharacterLimit * CharacterWidth, TextFieldHeight));

            NodeNameLabelRect = new Rect(
                new Vector2(NodeTitleLabelRect.xMin, NodeTitleLabelRect.max.y + LeftPanelVerticalPadding * 0.5f),
                NodeTitleLabelRect.size);
            NodeNameFieldRect = new Rect(
                new Vector2(NodeNameLabelRect.xMax * 0.5f + LeftPanelHorizontalPadding, NodeNameLabelRect.min.y + LeftPanelVerticalPadding * 0.5f),
                new Vector2(TextFieldCharacterLimit * CharacterWidth, TextFieldHeight));

            NodeDialogueLabelRect = new Rect(
                new Vector2(NodeNameLabelRect.xMin, NodeNameLabelRect.max.y + LeftPanelVerticalPadding * 0.5f),
                NodeNameLabelRect.size);
            NodeDialogueFieldRect = new Rect(
                new Vector2(NodeDialogueLabelRect.xMax * 0.5f + LeftPanelHorizontalPadding, NodeDialogueLabelRect.yMin + LeftPanelVerticalPadding * 0.5f),
                new Vector2(TextFieldCharacterLimit * CharacterWidth, TextAreaHeight));

            NodePortraitLabelRect = new Rect(
                new Vector2(NodeDialogueLabelRect.xMin, NodeDialogueFieldRect.yMax + LeftPanelVerticalPadding * 0.5f),
                NodeDialogueLabelRect.size);
            NodePortraitFieldRect = new Rect(
                new Vector2(NodePortraitLabelRect.xMax * 0.5f + LeftPanelHorizontalPadding, NodePortraitLabelRect.yMin + LeftPanelVerticalPadding * 0.5f),
                NodePortraitLabelRect.size);

            NodeNextLabelRect = new Rect(
                new Vector2(NodePortraitLabelRect.xMin, NodePortraitLabelRect.yMax + LeftPanelVerticalPadding * 0.5f),
                NodePortraitLabelRect.size);
            NodeNextFieldRect = new Rect(
                new Vector2(NodeNextLabelRect.xMin, NodeNextLabelRect.yMax + LeftPanelVerticalPadding * 0.5f),
                NodeNextLabelRect.size);
        }

        internal List<(string, Rect)> GetNextDialogueLayout(List<Guid> nodes, Dictionary<Guid, DialogueNode> _dict)
        {
            float x = NodeNextFieldRect.xMin;
            float y = NodeNextFieldRect.yMin;
            Vector2 scale = NodeNextFieldRect.size;
            List<(string, Rect)> layouts = new List<(string, Rect)>();
            for (int index = 0; index < nodes.Count; index++)
            {
                (string, Rect) layout = (
                    _dict[nodes[index]].Label,
                    new Rect(new Vector2(x, y + (index * scale.y)), scale)
                    );
                layouts.Add(layout);
            }
            return layouts;
        }
    }
}
