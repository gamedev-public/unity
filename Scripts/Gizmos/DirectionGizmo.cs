﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JGDT.Gizmos
{
    using Gizmos = UnityEngine.Gizmos;

    /// <summary>
    /// A component that draws a line in the direction specified.
    /// </summary>
    [RequireComponent(typeof(Transform))]
    public class DirectionGizmo : MonoBehaviour
    {
        [Tooltip("The direction of the line.")]
        public GizmoDirection Direction = GizmoDirection.Forward;
        [Tooltip("How long the line is.")]
        public float LineRenderLength = 0.5f;
        [Tooltip("What color to use to draw the line with.")]
        public Color DebugColor = Color.red;
        [Tooltip("Will turn off this component when entering Play mode.")]
        public bool DisableOnPlay = false;

        private Ray GetLine()
        {
            Vector3 _lineDirection = transform.GetDirectionFromEnum(Direction);
            return new Ray(transform.position, _lineDirection);
        }

        private void OnDrawGizmos()
        {

            if (Application.isPlaying == false)
            {
                Gizmos.color = DebugColor;
                Ray ray = GetLine();
                Gizmos.DrawRay(ray.origin, ray.direction * LineRenderLength);
            }
            else
            {
                if (DisableOnPlay == false)
                {
                    Gizmos.color = DebugColor;
                    Ray ray = GetLine();
                    Gizmos.DrawRay(ray.origin, ray.direction * LineRenderLength);
                }
            }
        }
    }
}
