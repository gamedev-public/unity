﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JGDT.Gizmos
{
    public enum GizmoDirection
    {
        Forward,
        LocalForward,
        Backwards,
        LocalBackwards,
        Left,
        LocalLeft,
        Right,
        LocalRight
    }

    public static class GizmoUtility
    {
        public static Vector3 GetDirectionFromEnum(this Transform transform, GizmoDirection direction)
        {
            switch (direction)
            {
                case GizmoDirection.LocalForward:
                    return transform.forward;
                case GizmoDirection.LocalBackwards:
                    return -transform.forward;
                case GizmoDirection.LocalLeft:
                    return -transform.right;
                case GizmoDirection.LocalRight:
                    return transform.right;
                case GizmoDirection.Forward:
                case GizmoDirection.Backwards:
                case GizmoDirection.Left:
                case GizmoDirection.Right:
                    return GizmoDirectionGlobalMap[direction];
            }
            return Vector3.zero;
        }

        private static readonly Dictionary<GizmoDirection, Vector3> GizmoDirectionGlobalMap = new Dictionary<GizmoDirection, Vector3>
        {
            {GizmoDirection.Forward, Vector3.forward },
            {GizmoDirection.Backwards, Vector3.back },
            {GizmoDirection.Left, Vector3.left },
            {GizmoDirection.Right, Vector3.right }
        };
    }
}
