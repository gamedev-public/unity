﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JGDT.CharacterController.TwoD.SimpleCharacterControllerRigid
{
    /// <summary>
    /// This component creates the simplest of character controllers with a <see cref="Rigidbody2D"/>.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class SimpleCharacterControllerRigid2D : MonoBehaviour
    {
        [Tooltip("The speed at which the character moves.")]
        public float Speed = 5f;
        [Space(20), Header("Movement Keys")]
        public KeyCode Left = KeyCode.A;
        public KeyCode Right = KeyCode.D;
        [Space(20), Header("Debugging")]
        public bool CanMove = true;
        public Rigidbody2D RBody;

        private void Start()
        {
            if (RBody == null)
            {
                Rigidbody2D body = GetComponent<Rigidbody2D>();
                if (body)
                {
                    RBody = body;
                }
                else
                {
                    Debug.LogError($"{this} could not find a RigidBody. The Component will disable.");
                    enabled = false;
                }
            }
            RBody.freezeRotation = true;
        }

        private void Update()
        {
            if (CanMove)
            {
                Vector3 velocity = Vector3.zero;
                if (Input.GetKey(Left)) velocity += -transform.right;
                if (Input.GetKey(Right)) velocity += transform.right;
                velocity = velocity.normalized;

                velocity *= Speed * Time.deltaTime;
                transform.position += velocity;
            }
        }

        private void OnValidate()
        {
            if (RBody == null)
            {
                Rigidbody2D body = GetComponent<Rigidbody2D>();
                if (body) RBody = body;
            }
        }
    }
}
