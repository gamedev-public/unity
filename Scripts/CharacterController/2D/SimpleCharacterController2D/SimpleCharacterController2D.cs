﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JGDT.CharacterController.TwoD.SimpleCharacterController2D
{
    /// <summary>
    /// This component creates the simplest of 2D character controllers. It does not care about collision or physics.
    /// </summary>
    public class SimpleCharacterController2D : MonoBehaviour
    {
        [Tooltip("The speed at which the character moves.")]
        public float Speed = 5f;
        [Space(20), Header("Movement Keys")]
        public KeyCode Left = KeyCode.A;
        public KeyCode Right = KeyCode.D;
        [Space(20), Header("Debugging")]
        public bool CanMove = true;

        private void Update()
        {
            if (CanMove)
            {
                Vector3 velocity = Vector3.zero;
                if (Input.GetKey(Left)) velocity += -transform.right;
                if (Input.GetKey(Right)) velocity += transform.right;
                velocity = velocity.normalized;

                velocity *= Speed * Time.deltaTime;
                transform.position += velocity;
            }
        }
    }
}
