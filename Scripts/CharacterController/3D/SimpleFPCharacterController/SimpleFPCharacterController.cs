﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JGDT.CharacterController.ThreeD.SimpleFPCharacterController
{
    /// <summary>
    /// This component creates the simplest of first person character controllers.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class SimpleFPCharacterController : MonoBehaviour
    {
        [Header("Setup")]
        public CursorLockMode CrosshairLockMode = CursorLockMode.Locked;
        [Space(20), Header("Movement Keys")]
        public KeyCode Forward = KeyCode.W;
        public KeyCode Back = KeyCode.S;
        public KeyCode Left = KeyCode.A;
        public KeyCode Right = KeyCode.D;
        [Space(20), Header("Speed/Sensitivity")]
        [Tooltip("The speed at which the character moves.")]
        public float Speed = 5f;
        [Range(0.05f, 1f), Tooltip("The sensitivity for horizontal mouse rotation.")]
        public float HorizontalTurnSensitivity = 1f;
        [Range(0.05f, 1f), Tooltip("The sensitivity for vertical mouse rotation.")]
        public float VerticalTurnSensitivity = 1f;
        [Space(20), Header("Debugging")]
        public bool CanMove = true;
        public bool CanRotate = true;
        public Rigidbody RBody;
        public Camera PlayerCamera;

        // Private Fields
        private float _xRotation;
        private Vector2 _turningThreshold = new Vector2(-75f, 75f);

        private void Start()
        {
            Cursor.lockState = CrosshairLockMode;

            GameObject CameraObject = new GameObject("Player Camera", typeof(Camera));
            CameraObject.transform.parent = transform;
            CameraObject.transform.localPosition = Vector3.zero;
            PlayerCamera = CameraObject.GetComponent<Camera>();
            PlayerCamera.tag = "MainCamera";

            if (RBody == null)
            {
                Rigidbody rbody = GetComponent<Rigidbody>();
                if (rbody)
                {
                    RBody = rbody;
                }
                else
                {
                    Debug.LogError($"{this} could not find a Rigidbody. The Component will disable.");
                    enabled = false;
                }
            }
            RBody.freezeRotation = true;
        }

        private void Update()
        {
            if (CanRotate)
            {
                float horizontal = Input.GetAxis("Mouse X") * HorizontalTurnSensitivity;
                float vertical = Input.GetAxis("Mouse Y") * VerticalTurnSensitivity;

                _xRotation -= vertical;
                _xRotation = Mathf.Clamp(_xRotation, _turningThreshold.x, _turningThreshold.y);
                PlayerCamera.transform.localRotation = Quaternion.Euler(_xRotation, 0, 0);
                transform.Rotate(Vector3.up * horizontal);
            }
        }

        private void FixedUpdate()
        {
            if (CanMove)
            {
                Vector3 movement = Vector3.zero;
                if (Input.GetKey(Forward)) movement += transform.forward;
                if (Input.GetKey(Back)) movement += -transform.forward;
                if (Input.GetKey(Left)) movement += -transform.right;
                if (Input.GetKey(Right)) movement += transform.right;
                movement = movement.normalized;

                if (movement.sqrMagnitude > 0.0001f)
                {
                    movement *= Speed * Time.deltaTime;
                    transform.position += movement;
                }
            }
        }

        private void OnValidate()
        {
            if (RBody == null) RBody = GetComponent<Rigidbody>();
        }
    }
}
