﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JGDT.CharacterController.ThreeD.SimpleCharacterControllerRigid
{
    /// <summary>
    /// This component creates the simplest of character controllers with a <see cref="Rigidbody"/>.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class SimpleCharacterControllerRigid : MonoBehaviour
    {
        [Tooltip("The speed at which the character moves.")]
        public float Speed = 5f;
        [Space(20), Header("Movement Keys")]
        public KeyCode Forward = KeyCode.W;
        public KeyCode Back = KeyCode.S;
        public KeyCode Left = KeyCode.A;
        public KeyCode Right = KeyCode.D;
        [Space(20), Header("Debugging")]
        public bool CanMove = true;
        public Rigidbody RBody;

        private void Start()
        {
            if (RBody == null)
            {
                Rigidbody body = GetComponent<Rigidbody>();
                if (body)
                {
                    RBody = body;
                }
                else
                {
                    Debug.LogError($"{this} could not find a RigidBody. The Component will disable.");
                    enabled = false;
                }
            }
            RBody.freezeRotation = true;
        }

        private void Update()
        {
            if (CanMove)
            {
                Vector3 velocity = Vector3.zero;
                if (Input.GetKey(Forward)) velocity += transform.forward;
                if (Input.GetKey(Back)) velocity += -transform.forward;
                if (Input.GetKey(Left)) velocity += -transform.right;
                if (Input.GetKey(Right)) velocity += transform.right;
                velocity = velocity.normalized;

                velocity *= Speed * Time.deltaTime;
                transform.position += velocity;
            }
        }

        private void OnValidate()
        {
            if (RBody == null)
            {
                Rigidbody body = GetComponent<Rigidbody>();
                if (body) RBody = body;
            }
        }
    }
}
